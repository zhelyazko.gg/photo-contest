package com.example.photocontest.repositories;

import com.example.photocontest.models.UserRank;
import com.example.photocontest.repositories.contracts.RankRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RankRepositoryImpl extends AbstractReadRepository<UserRank> implements RankRepository {
    public RankRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRank.class, sessionFactory);
    }
}
