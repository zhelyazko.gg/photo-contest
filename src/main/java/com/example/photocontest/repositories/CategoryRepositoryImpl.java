package com.example.photocontest.repositories;

import com.example.photocontest.models.Category;
import com.example.photocontest.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepository<Category> implements CategoryRepository {

    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
    }
}
