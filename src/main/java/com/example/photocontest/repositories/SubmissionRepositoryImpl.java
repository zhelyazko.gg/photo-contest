package com.example.photocontest.repositories;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.SubmissionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubmissionRepositoryImpl extends AbstractCRUDRepository<Submission> implements SubmissionRepository {

    public static final int FIRST_ELEMENT_INDEX = 0;
    private final SessionFactory sessionFactory;

    public SubmissionRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(Submission.class, sessionFactory);
        this.sessionFactory = sessionFactory1;
    }

    @Override
    public Submission getParticipant(User user, Contest contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Submission> query = session.createQuery("from Submission where contest_id = :contest_id " +
                    "and participant = :userId", Submission.class);
            query.setParameter("contest_id", contestId);
            query.setParameter("userId", user);

            List<Submission> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", user.getUsername());
            }
            return result.get(FIRST_ELEMENT_INDEX);
        }
    }

    @Override
    public List<Submission> getSubOrdered(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Query<Submission> query = session.createQuery("from Submission where contest_id = :contest_id order by points desc ", Submission.class);
            query.setParameter("contest_id", contest);
            return query.getResultList();
        }
    }
}
