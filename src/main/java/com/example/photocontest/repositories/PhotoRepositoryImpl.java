package com.example.photocontest.repositories;

import com.example.photocontest.models.Photo;
import com.example.photocontest.repositories.contracts.PhotoRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class PhotoRepositoryImpl extends AbstractCRUDRepository<Photo> implements PhotoRepository {
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(Photo.class, sessionFactory);
    }
}
