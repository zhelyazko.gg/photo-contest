package com.example.photocontest.repositories.contracts;

public interface BaseCRUDRepository<T> extends BaseReadRepository<T> {

    void delete(int id);

    T create(T entity);

    void update(T entity);
}
