package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.UserRank;

public interface RankRepository extends BaseReadRepository<UserRank> {

}
