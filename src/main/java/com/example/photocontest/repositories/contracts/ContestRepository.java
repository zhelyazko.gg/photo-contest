package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.ContestSortOption;

import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseCRUDRepository<Contest> {

    int countContests();

    List<Contest> search(Optional<String> title, Optional<String> category,
                         Optional<String> phase,
                         Optional<ContestSortOption> sortOption);

    List<Contest> countFinishedContests();

    List<Contest> getOpenContests();

    List<Contest> getPhase1Contests();

    List<Contest> mostParticipatoryContest();

    List<Contest> newestContests();

    List<Contest> getPhase1ContestsWhereUserIsNotJury(User user);
}
