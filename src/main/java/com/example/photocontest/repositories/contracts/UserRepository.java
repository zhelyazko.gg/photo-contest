package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserSortOption;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseCRUDRepository<User> {

    User getByUsername(String username);

    List<User> search(Optional<String> username, Optional<String> first_name,
                      Optional<String> last_name,
                      Optional<UserSortOption> sort);//, Optional<String> firstname,Optional<String> email

    Long countUsers();

    int countOrganizers();

    User findByEmail(String email);

    User findByResetPasswordToken(String token);

    List<User> getUsersForJury();


}
