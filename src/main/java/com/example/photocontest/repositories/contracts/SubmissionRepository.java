package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.User;

import java.util.List;

public interface SubmissionRepository extends BaseCRUDRepository<Submission> {

    Submission getParticipant(User user, Contest contestId);

    List<Submission> getSubOrdered(Contest contest);
}
