package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Rating;

public interface RatingRepository extends BaseReadRepository<Rating> {

}
