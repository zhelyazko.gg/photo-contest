package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionScoring;
import com.example.photocontest.models.User;

public interface SubmissionScoringRepository extends BaseCRUDRepository<SubmissionScoring> {
    SubmissionScoring voted(User user, Submission submission);

}
