package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Phase;

public interface PhaseRepository extends BaseReadRepository<Phase> {
}
