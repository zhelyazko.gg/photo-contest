package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Photo;

public interface PhotoRepository extends BaseCRUDRepository<Photo> {
}
