package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Category;

public interface CategoryRepository extends BaseCRUDRepository<Category> {

}
