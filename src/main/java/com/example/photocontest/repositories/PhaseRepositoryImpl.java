package com.example.photocontest.repositories;

import com.example.photocontest.models.Phase;
import com.example.photocontest.repositories.contracts.PhaseRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class PhaseRepositoryImpl extends AbstractReadRepository<Phase> implements PhaseRepository {
    public PhaseRepositoryImpl(SessionFactory sessionFactory) {
        super(Phase.class, sessionFactory);
    }
}
