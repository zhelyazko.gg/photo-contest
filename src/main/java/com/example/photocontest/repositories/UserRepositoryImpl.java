package com.example.photocontest.repositories;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserSortOption;
import com.example.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {
    public static final int FIRST_ELEMENT_INDEX = 0;
    private final SessionFactory sessionFactory;


    public UserRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory1;
    }

    @Override
    public List<User> search(Optional<String> username, Optional<String> first_name,
                             Optional<String> last_name,
                             Optional<UserSortOption> sort) {

        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("from User ");
            var search = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            username.ifPresent(value -> {
                search.add(" username like :username");
                queryParams.put("username", "%" + value + "%");
            });
            first_name.ifPresent(value -> {
                search.add(" first_name like :first_name");
                queryParams.put("first_name", "%" + value + "%");
            });
            last_name.ifPresent(value -> {
                search.add(" last_name like :last_name");
                queryParams.put("last_name", "%" + value + "%");
            });

            if (!search.isEmpty()) {
                queryString.append("where ").append(String.join(" and ", search));
            }

            sort.ifPresent(value -> queryString.append(value.getQuery()));

            return session.createQuery(queryString.toString(), User.class)
                    .setProperties(queryParams)
                    .list();
        }

    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(FIRST_ELEMENT_INDEX);
        }
    }


    @Override
    public User findByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> result = query.list();
            if (result.isEmpty()){
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(FIRST_ELEMENT_INDEX);
        }
    }


    @Override
    public Long countUsers() {
        return (long) getAll().size();

    }

    @Override
    public int countOrganizers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where rank_id = 1", User.class);
            return query.getResultList().size();
        }
    }

    @Override
    public User findByResetPasswordToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where resetPasswordToken = :resetPasswordToken", User.class);
            query.setParameter("resetPasswordToken", token);
            List<User> result = query.list();
            if (result.isEmpty()){
                throw new EntityNotFoundException("User", "token", token);
            }
            return result.get(FIRST_ELEMENT_INDEX);
        }
    }

    @Override
    public List<User> getUsersForJury() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where rank_id = 4 or rank_id = 5", User.class);
            return query.getResultList();
        }
    }
}


