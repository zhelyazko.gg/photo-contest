package com.example.photocontest.repositories;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionScoring;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.SubmissionScoringRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubmissionScoringRepositoryImpl extends AbstractCRUDRepository<SubmissionScoring> implements SubmissionScoringRepository {

    public static final int FIRST_ELEMENT_INDEX = 0;

    public SubmissionScoringRepositoryImpl(SessionFactory sessionFactory) {
        super(SubmissionScoring.class, sessionFactory);
    }

    @Override
    public SubmissionScoring voted(User user, Submission submission) {
        try (Session session = sessionFactory.openSession()) {
            Query<SubmissionScoring> query = session.createQuery("from SubmissionScoring where submission = :submission " +
                    "and user = :user", SubmissionScoring.class);
            query.setParameter("submission", submission);
            query.setParameter("user", user);
            List<SubmissionScoring> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", user.getUsername());
            }
            return result.get(FIRST_ELEMENT_INDEX);
        }
    }

}
