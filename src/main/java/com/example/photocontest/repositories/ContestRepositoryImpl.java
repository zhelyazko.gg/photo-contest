package com.example.photocontest.repositories;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.ContestSortOption;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.services.contracts.PhaseService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {

    public static final int CONTEST_ON_FIRST_PAGE_LIMIT = 4;
    public static final int PHASE_FINISH_ID = 3;
    public static final int PHASE_OPEN_ID = 1;
    private final SessionFactory sessionFactory;
    public final PhaseService phaseService;

    public ContestRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1, PhaseService phaseService) {
        super(Contest.class, sessionFactory);
        this.sessionFactory = sessionFactory1;
        this.phaseService = phaseService;
    }

    @Override
    public List<Contest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where deleted = false ", Contest.class);
            return query.list();
        }
    }

    @Override
    public int countContests() {
        return getAll().size();
    }

    @Override
    public List<Contest> search(Optional<String> title,
                                Optional<String> category,
                                Optional<String> phase,
                                Optional<ContestSortOption> sortOption) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("from Contest ");
            var search = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            title.ifPresent(value -> {
                search.add(" title like :title");
                queryParams.put("title", "%" + value + "%");
            });
            category.ifPresent(value -> {
                search.add(" category_id like :category_id");
                queryParams.put("category_id", "%" + value + "%");
            });
            phase.ifPresent(value -> {
                search.add(" phase_id like :phase_id");
                queryParams.put("phase_id", "%" + value + "%");
            });

            if (!search.isEmpty()) {
                queryString.append("where ").append(String.join(" and ", search));
            }

            sortOption.ifPresent(value -> queryString.append(value.getQuery()));

            return session.createQuery(queryString.toString(), Contest.class)
                    .setProperties(queryParams)
                    .list();
        }
    }

    @Override
    public List<Contest> countFinishedContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where phase = :phase", Contest.class);
            query.setParameter("phase", phaseService.getById(PHASE_FINISH_ID));
            List<Contest> result = query.list();
            return result;
        }
    }

    public List<Contest> getOpenContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where phase != :phase", Contest.class);
            query.setParameter("phase", phaseService.getById(PHASE_FINISH_ID));
            return query.list();
        }
    }

    public List<Contest> getPhase1Contests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where phase = :phase", Contest.class);
            query.setParameter("phase", phaseService.getById(PHASE_OPEN_ID));
            return query.list();
        }
    }

    public List<Contest> getPhase1ContestsWhereUserIsNotJury(User user){
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where phase = :phase", Contest.class);
            query.setParameter("phase", phaseService.getById(1));
            List<Contest> contests = query.list();
            contests.removeIf(contest -> user.getJury().contains(contest));
            return contests;
        }
    }

    @Override
    public List<Contest> mostParticipatoryContest() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest c order by size(c.submissions) desc",
                    Contest.class);
            query.setMaxResults(CONTEST_ON_FIRST_PAGE_LIMIT);
            return query.list();
        }
    }

    @Override
    public List<Contest> newestContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest c order by day (c.creation) desc",
                    Contest.class);
            query.setMaxResults(CONTEST_ON_FIRST_PAGE_LIMIT);
            return query.list();
        }
    }
}
