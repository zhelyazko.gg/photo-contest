package com.example.photocontest.repositories;

import com.example.photocontest.models.Rating;
import com.example.photocontest.repositories.contracts.RatingRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RatingRepositoryImpl extends AbstractReadRepository<Rating> implements RatingRepository {
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        super(Rating.class, sessionFactory);
    }
}
