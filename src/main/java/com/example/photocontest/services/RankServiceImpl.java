package com.example.photocontest.services;

import com.example.photocontest.models.UserRank;
import com.example.photocontest.repositories.contracts.RankRepository;
import com.example.photocontest.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RankServiceImpl implements RankService {

    private final RankRepository repository;

    @Autowired
    public RankServiceImpl(RankRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<UserRank> getAll() {
        return repository.getAll();
    }

    @Override
    public UserRank getById(int id) {
        return repository.getById(id);
    }
}
