package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionScoring;
import com.example.photocontest.models.User;

public interface SubmissionScoringService {

    SubmissionScoring create(SubmissionScoring submissionScoring, User user, Submission submission);

    void update(SubmissionScoring submissionScoring, User user, int id);

    SubmissionScoring getById(int id);

    boolean voted(User user, int subId);

//    int getScore(int id);

}
