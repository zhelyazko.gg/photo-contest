package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Phase;

import java.util.List;

public interface PhaseService {
    List<Phase> getAll();

    Phase getById(int id);
}
