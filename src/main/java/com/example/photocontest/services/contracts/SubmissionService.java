package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Submission;
import com.example.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SubmissionService {

    List<Submission> getAll();

    Submission getById(int id);

    Submission create(Submission submission, User user, int contest_id, MultipartFile multipartFile);

    void delete(int id, User user);

    void update(Submission submission);

    boolean participate(User user, int contestId);

    void setWinners(int id);

}
