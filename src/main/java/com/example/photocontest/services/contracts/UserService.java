package com.example.photocontest.services.contracts;

import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserSortOption;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getUsers();

    List<User> getUsersForJury();

    User getById(int id);

    User getByUsername(String username);

    User create(User user);

    void update(User newUser, User authenticate, MultipartFile multipartFile);

    void update(User newUser, User authenticate);

    List<User> search(Optional<String> username, Optional<String> firstName, Optional<String> lastName, Optional<UserSortOption> sortOptions);

    String promote(User user, int id);

    String demote(User user, int id);

    Long countUsers();

    int countOrganizers();

    void updateRank(User user);

    void updateResetPasswordToken(String token, String email);

    User getByResetPasswordToken(String token);

    void updatePassword(User user, String password);
}
