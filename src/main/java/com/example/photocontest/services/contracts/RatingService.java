package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Rating;

import java.util.List;

public interface RatingService {
    List<Rating> getAll();

    Rating getById(int rating_id);

}
