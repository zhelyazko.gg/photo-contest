package com.example.photocontest.services.contracts;

import com.example.photocontest.models.UserRank;

import java.util.List;

public interface RankService {
    List<UserRank> getAll();

    UserRank getById(int id);
}
