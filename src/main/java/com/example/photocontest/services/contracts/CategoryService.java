package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Category;
import com.example.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();

    Category getById(int id);

    void create(Category category, User user, MultipartFile multipartFile);

    void create(Category category, User user);

}
