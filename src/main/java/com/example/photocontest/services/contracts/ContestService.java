package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.ContestSortOption;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface ContestService {

    List<Contest> getAll();

    List<Contest> getPhase1Contests(User user);

    Contest getById(int id);

    Contest create(Contest contest, User user, MultipartFile multipartFile);

    void update(Contest contest, User user, MultipartFile multipartFile);

    void update(Contest contest, User user);

    void setPhase();

    int countContests();

    int countOpenContests();

    int countFinishedContests();

    void delete(int id, User user);

    List<Contest> mostParticipatoryContest();

    List<Contest> newestContests();

    void setJury(User user, Contest contest);

    List<Contest> search(Optional<String> title, Optional<String> category,
                         Optional<String> phase, Optional<ContestSortOption> sortOptions);
}
