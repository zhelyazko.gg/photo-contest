package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PhotoService {

    Photo create(MultipartFile multipartFile, User user) throws IOException;

    Photo getById(int id);
}
