package com.example.photocontest.services;

import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.repositories.contracts.SubmissionRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.SubmissionService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.helpers.AuthorizationHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@Service
public class SubmissionServiceImpl implements SubmissionService {

    public static final int INDEX_OF_FIRST_ELEMENT = 0;
    public static final int JOIN_CONTEST_POINTS = 1;
    private final SubmissionRepository submissionRepository;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final UserRepository userRepository;
    private final UserService userService;
    private final ContestRepository contestRepository;

    @Autowired
    public SubmissionServiceImpl(SubmissionRepository submissionRepository,
                                 ContestService contestService, PhotoService photoService, UserRepository userRepository, UserService userService, ContestRepository contestRepository) {
        this.submissionRepository = submissionRepository;
        this.contestService = contestService;
        this.photoService = photoService;
        this.userRepository = userRepository;
        this.userService = userService;
        this.contestRepository = contestRepository;
    }

    @Override
    public List<Submission> getAll() {
        return submissionRepository.getAll();
    }

    @Override
    public Submission getById(int id) {
        return submissionRepository.getById(id);
    }

    @Override
    public boolean participate(User user, int contestId) {
        Contest contest = contestService.getById(contestId);
        try {
            submissionRepository.getParticipant(user, contest);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @Override
    public void setWinners(int id) {
        Contest contest = contestService.getById(id);
        List<Submission> ordered = submissionRepository.getSubOrdered(contest);
        setFirstPlace(contest, ordered);
        setSecondPlace(contest, ordered);
        setThirdPlace(contest, ordered);
        contestRepository.update(contest);
    }

    private void setThirdPlace(Contest contest, List<Submission> ordered) {
        if (!ordered.isEmpty()) {
            List<Submission> thirdPlace = contest.getWinners();
            int counter = thirdPlace.size();
            thirdPlace.add(ordered.get(INDEX_OF_FIRST_ELEMENT));
            User user2 = ordered.get(INDEX_OF_FIRST_ELEMENT).getParticipant();
            user2.setPoints(user2.getPoints() + 20);
            userRepository.update(user2);
            for (int i = 1; i <= ordered.size() - 1; i++) {
                User temp = ordered.get(i).getParticipant();
                if (thirdPlace.get(thirdPlace.size() - 1).getPoints() == ordered.get(i).getPoints()) {
                    thirdPlace.add(ordered.get(i));
                    temp.setPoints(temp.getPoints() + 10);
                    userRepository.update(temp);
                    userService.updateRank(temp);
                } else {
                    break;
                }
            }
            if (thirdPlace.size() - counter > 1) {
                user2.setPoints(user2.getPoints() - 10);
                userRepository.update(user2);
            }
            userService.updateRank(user2);
            contest.setWinners(thirdPlace);
        }
    }

    private void setSecondPlace(Contest contest, List<Submission> ordered) {
        if (!ordered.isEmpty()) {
            List<Submission> secondPlace = contest.getWinners();
            int firstPlaceSize = secondPlace.size();
            int firstPlacePoints = secondPlace.get(INDEX_OF_FIRST_ELEMENT).getPoints();
            User winner = secondPlace.get(INDEX_OF_FIRST_ELEMENT).getParticipant();
            secondPlace.add(ordered.get(INDEX_OF_FIRST_ELEMENT));
            User user1 = ordered.get(INDEX_OF_FIRST_ELEMENT).getParticipant();
            user1.setPoints(user1.getPoints() + 35);
            userRepository.update(user1);
            for (int i = 1; i < ordered.size() - 1; i++) {
                User temp = ordered.get(i).getParticipant();
                if (secondPlace.get(secondPlace.size() - 1).getPoints() == ordered.get(i).getPoints()) {
                    secondPlace.add(ordered.get(i));
                    temp.setPoints(temp.getPoints() + 25);
                    userRepository.update(temp);
                    userService.updateRank(temp);
                } else {
                    break;
                }
            }
            if (secondPlace.size() - firstPlaceSize > 1) {
                user1.setPoints(user1.getPoints() - 10);
                userRepository.update(user1);
            }
            contest.setWinners(secondPlace);
            if (firstPlacePoints >= secondPlace.get(secondPlace.size() - 1).getPoints() * 2
                    && firstPlaceSize == 1) {
                winner.setPoints(winner.getPoints() + 25);
                userRepository.update(winner);
            }
            userService.updateRank(winner);
            userService.updateRank(user1);
            ordered.subList(0, secondPlace.size() - firstPlaceSize).clear();
        }
    }

    private void setFirstPlace(Contest contest, List<Submission> ordered) {
        if (!ordered.isEmpty()) {
            List<Submission> firstPlace = contest.getWinners();
            firstPlace.add(ordered.get(INDEX_OF_FIRST_ELEMENT));
//            firstPlace.get(INDEX_OF_FIRST_ELEMENT).setWinnerPlace(1);
            User user = firstPlace.get(INDEX_OF_FIRST_ELEMENT).getParticipant();
            user.setPoints(user.getPoints() + 50);
            userRepository.update(user);
            for (int i = 1; i < ordered.size() - 1; i++) {
                User temp = ordered.get(i).getParticipant();
                if (firstPlace.get(firstPlace.size() - 1).getPoints() == ordered.get(i).getPoints()) {
                    firstPlace.add(ordered.get(i));
//                    firstPlace.get(firstPlace.size() - 1).setWinnerPlace(1);
                    temp.setPoints(temp.getPoints() + 40);
                    userRepository.update(temp);
                    userService.updateRank(temp);
                } else {
                    break;
                }
            }
            if (firstPlace.size() > 1) {
                user.setPoints(user.getPoints() - 10);
                userRepository.update(user);
            }
            userService.updateRank(user);
            contest.setWinners(firstPlace);
            ordered.subList(0, firstPlace.size()).clear();
        }
    }

    @Override
    public Submission create(Submission submission, User user, int contest_id, MultipartFile multipartFile) {

        Contest contest = contestService.getById(contest_id);
        boolean participantExists = true;
        try {
            submissionRepository.getParticipant(user, contest);
        } catch (EntityNotFoundException e) {
            participantExists = false;
        }
        if (participantExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        Photo photo;
        try {
            photo = photoService.create(multipartFile, user);
        } catch (IOException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        submission.setPhoto(photo);
        Set<Submission> submissions = contest.getSubmissions();
        submissions.add(submission);
        submission.setContest_id(contest);
        user.setPoints(user.getPoints() + JOIN_CONTEST_POINTS);
        submission.setParticipant(user);
        Set<Contest> contests = user.getContests();
        contests.add(contest);
        user.setContests(contests);
        userRepository.update(user);
        userService.updateRank(user);
        return submissionRepository.create(submission);
    }

    public void update(Submission submission) {
        submissionRepository.update(submission);
    }

    @Override
    public void delete(int id, User user) {
        AuthorizationHelpers.authorizeOrganizer(user);
        Submission submission = getById(id);
        User participant = submission.getParticipant();
        participant.setPoints(participant.getPoints() - JOIN_CONTEST_POINTS);
        userRepository.update(user);
        submissionRepository.delete(id);
    }
}
