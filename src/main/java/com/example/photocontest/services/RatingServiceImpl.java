package com.example.photocontest.services;

import com.example.photocontest.models.Rating;
import com.example.photocontest.repositories.contracts.RatingRepository;
import com.example.photocontest.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository repository;

    @Autowired
    public RatingServiceImpl(RatingRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Rating> getAll() {
        return repository.getAll();
    }

    @Override
    public Rating getById(int rating_id) {
        return repository.getById(rating_id);
    }
}
