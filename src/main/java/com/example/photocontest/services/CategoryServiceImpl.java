package com.example.photocontest.services;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Category;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.CategoryRepository;
import com.example.photocontest.services.contracts.CategoryService;
import com.example.photocontest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final PhotoService photoService;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, PhotoService photoService) {
        this.categoryRepository = categoryRepository;
        this.photoService = photoService;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public void create(Category category, User user, MultipartFile multipartFile) {
        Photo photo;
        try {
            photo = photoService.create(multipartFile, user);
        } catch (IOException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        category.setPhoto(photo);
        categoryRepository.create(category);
    }

    @Override
    public void create(Category category, User user) {
        categoryRepository.create(category);
    }


}
