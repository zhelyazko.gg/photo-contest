package com.example.photocontest.services;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserSortOption;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.helpers.AuthorizationHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    public static final int RANK_ORGANIZER_ID = 1;
    public static final int RANK_JUNKIE_ID = 2;
    public static final int RANK_ENTHUSIAST_ID = 3;
    public static final int RANK_MASTER_ID = 4;
    public static final int RANK_WISE_AND_BENEVOLENT_PHOTO_DICTATOR = 5;
    private final UserRepository repository;
    private final PhotoService photoService;

    @Autowired
    public UserServiceImpl(UserRepository repository, @Lazy PhotoService photoService) {
        this.repository = repository;
        this.photoService = photoService;
    }

    @Override
    public List<User> getUsers() {
        return repository.getAll();
    }

    @Override
    public List<User> getUsersForJury() {
        return repository.getUsersForJury();
    }

    @Override
    public List<User> search(Optional<String> username, Optional<String> first_name, Optional<String> last_name, Optional<UserSortOption> sort) {
        return repository.search(username, first_name, last_name, sort);
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public User create(User user) {
        return repository.create(user);
    }

    @Override
    public void update(User newUser, User authenticate, MultipartFile multipartFile) {
        AuthorizationHelpers.authorizeSameUser(newUser, authenticate);
        Photo photo;
        try {
            photo = photoService.create(multipartFile, newUser);
        } catch (IOException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        newUser.setPhoto(photo);
        repository.update(newUser);
    }

    @Override
    public void update(User newUser, User authenticate) {
        AuthorizationHelpers.authorizeSameUser(newUser, authenticate);
        repository.update(newUser);
    }

    @Override
    public String promote(User user, int id) {
        AuthorizationHelpers.authorizeOrganizer(user);
        User userToBePromoted = repository.getById(id);
        userToBePromoted.setRank_id(RANK_ORGANIZER_ID);
        repository.update(userToBePromoted);
        return String.format("%s has been promoted.", userToBePromoted.getUsername());
    }

    @Override
    public String demote(User user, int id) {
        AuthorizationHelpers.authorizeOrganizer(user);
        User userToBeDemoted = repository.getById(id);
        userToBeDemoted.setRank_id(RANK_JUNKIE_ID);
        repository.update(userToBeDemoted);
        return String.format("%s has been demoted.", userToBeDemoted.getUsername());
    }

    @Override
    public Long countUsers() {
        return repository.countUsers();
    }

    @Override
    public int countOrganizers() {
        return repository.countOrganizers();
    }

    @Override
    public void updateRank(User user) {
        if (user.getRank_id() == RANK_JUNKIE_ID && user.getPoints() > 50) {
            user.setRank_id(RANK_ENTHUSIAST_ID);
        } else if (user.getRank_id() == RANK_ENTHUSIAST_ID && user.getPoints() > 150) {
            user.setRank_id(RANK_MASTER_ID);
        } else if (user.getRank_id() == RANK_MASTER_ID && user.getPoints() > 1000) {
            user.setRank_id(RANK_WISE_AND_BENEVOLENT_PHOTO_DICTATOR);
        }
        repository.update(user);
    }

    public void updateResetPasswordToken(String token, String email) throws EntityNotFoundException {
        User user = repository.findByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            repository.update(user);
        } else {
            throw new EntityNotFoundException("Could not find any customer with the email " + email);
        }
    }

    public User getByResetPasswordToken(String token) {
        return repository.findByResetPasswordToken(token);
    }

    public void updatePassword(User user, String newPassword) {
        user.setPassword(newPassword);
        user.setResetPasswordToken(null);
        repository.update(user);
    }

}