package com.example.photocontest.services;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionScoring;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.SubmissionScoringRepository;
import com.example.photocontest.services.contracts.SubmissionScoringService;
import com.example.photocontest.services.contracts.SubmissionService;
import com.example.photocontest.utils.helpers.AuthorizationHelpers;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class SubmissionScoringServiceImpl implements SubmissionScoringService {

    private final SubmissionScoringRepository submissionScoringRepository;
    private final SubmissionService submissionService;

    public SubmissionScoringServiceImpl(SubmissionScoringRepository submissionScoringRepository, SubmissionService submissionService) {
        this.submissionScoringRepository = submissionScoringRepository;
        this.submissionService = submissionService;
    }

    @Override
    public SubmissionScoring create(SubmissionScoring submissionScoring, User user, Submission submission) {
        submissionScoring.setUser(user);
        submissionScoring.setSubmission(submission);

        submission.setPoints(submission.getPoints() + submissionScoring.getRating().getScore());
        submissionService.update(submission);
        Set<SubmissionScoring> scoring = submission.getScoring();
        scoring.add(submissionScoring);
        return submissionScoringRepository.create(submissionScoring);
    }

    @Override
    public void update(SubmissionScoring submissionScoring, User user, int id) {
        User creator = submissionScoring.getUser();
        AuthorizationHelpers.authorizeSameUser(creator, user);
        Submission submission = submissionScoring.getSubmission();
        submission.setPoints(submission.getPoints() - getById(id).getRating().getScore()
                + submissionScoring.getRating().getScore());
        submissionService.update(submission);
        Set<SubmissionScoring> scorings = submission.getScoring();
        scorings.remove(getById(id));
        scorings.add(submissionScoring);
        submissionScoringRepository.update(submissionScoring);
    }

    @Override
    public SubmissionScoring getById(int id) {
        return submissionScoringRepository.getById(id);
    }

    @Override
    public boolean voted(User user, int subId) {
        Submission submission = submissionService.getById(subId);
        try {
            submissionScoringRepository.voted(user, submission);
        } catch (EntityNotFoundException e) {
            return true;
        }
        return false;
    }

}
