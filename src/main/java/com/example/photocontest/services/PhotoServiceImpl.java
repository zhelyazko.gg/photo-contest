package com.example.photocontest.services;

import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.PhotoRepository;
import com.example.photocontest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public Photo create(MultipartFile multipartFile, User user) throws IOException {
        Photo photo = new Photo();
        photo.setUser(user);
        photo.setFileName(multipartFile.getOriginalFilename());
        Path currentPath = Paths.get(".");
        Path absolutePath = currentPath.toAbsolutePath();
        photo.setPath(absolutePath + "/src/main/resources/static/img/");
        byte[] bytes = multipartFile.getBytes();
        Path path = Paths.get(photo.getPath() + multipartFile.getOriginalFilename());
        Files.write(path, bytes);
        photoRepository.create(photo);
        return photo;
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }
}
