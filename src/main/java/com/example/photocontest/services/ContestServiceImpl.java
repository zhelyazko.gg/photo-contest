package com.example.photocontest.services;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.ContestSortOption;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.PhaseService;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.SubmissionService;
import com.example.photocontest.utils.helpers.AuthorizationHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ContestServiceImpl implements ContestService {

    public static final int SECOND_PHASE_ID = 2;
    public static final int THIRD_PHASE_ID = 3;
    private final ContestRepository contestRepository;
    private final PhotoService photoService;
    private final PhaseService phaseService;
    private final SubmissionService submissionService;
    private final UserRepository userRepository;


    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, PhotoService photoService, PhaseService phaseService, @Lazy SubmissionService submissionService, UserRepository userRepository) {
        this.contestRepository = contestRepository;
        this.photoService = photoService;
        this.phaseService = phaseService;
        this.submissionService = submissionService;
        this.userRepository = userRepository;
    }

    @Override
    public List<Contest> getAll() {
        return contestRepository.getAll();
    }

    public List<Contest> getPhase1Contests(User user) {
        return contestRepository.getPhase1ContestsWhereUserIsNotJury(user);
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public Contest create(Contest contest, User user, MultipartFile multipartFile) {
        setParamsCU(contest, user, multipartFile);
        return contestRepository.create(contest);
    }


    @Override
    public void update(Contest contest, User user, MultipartFile multipartFile) {
        setParamsCU(contest, user, multipartFile);
        contestRepository.update(contest);
    }

    @Override
    public void update(Contest contest, User user) {
        AuthorizationHelpers.authorizeOrganizer(user);
        contestRepository.update(contest);
    }

    private void setParamsCU(Contest contest, User user, MultipartFile multipartFile) {
        AuthorizationHelpers.authorizeOrganizer(user);
        Photo photo;
        try {
            photo = photoService.create(multipartFile, user);
        } catch (IOException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        contest.setPhoto(photo);
        contest.setOrganizer(user);
    }

    public void setPhase() {
        if (!contestRepository.getOpenContests().isEmpty()) {
            List<Contest> contests = contestRepository.getOpenContests();
            for (Contest contest : contests) {
                if (LocalDateTime.now().isAfter(contest.getPhase1()) && LocalDateTime.now().isBefore(contest.getPhase2())) {
                    contest.setPhase(phaseService.getById(SECOND_PHASE_ID));
                    contestRepository.update(contest);
                } else if (LocalDateTime.now().isAfter(contest.getPhase2())) {
                    contest.setPhase(phaseService.getById(THIRD_PHASE_ID));
                    submissionService.setWinners(contest.getId());
                    contestRepository.update(contest);
                }
            }
        }
    }

    @Override
    public void delete(int id, User user) {
        AuthorizationHelpers.authorizeOrganizer(user);
        Contest contest = getById(id);
        contest.setDeleted(true);
        contestRepository.update(contest);
    }

    @Override
    public List<Contest> mostParticipatoryContest() {
        return contestRepository.mostParticipatoryContest();
    }

    @Override
    public List<Contest> newestContests() {
        return contestRepository.newestContests();
    }

    @Override
    public void setJury(User user, Contest contest) {
        Set<Contest> jury = user.getJury();
        if (jury.contains(contest)) {
            jury.remove(contest);
        } else {
            jury.add(contest);
        }
        user.setJury(jury);
        userRepository.update(user);
    }

    @Override
    public List<Contest> search(Optional<String> title, Optional<String> category, Optional<String> phase, Optional<ContestSortOption> sortOptions) {
        return contestRepository.search(title, category, phase, sortOptions);
    }

    @Override
    public int countContests() {
        return contestRepository.countContests();
    }

    @Override
    public int countOpenContests() {
        List<Contest> openContests = contestRepository.getPhase1Contests();
        if (openContests.isEmpty()) {
            return 0;
        } else return openContests.size();
    }

    @Override
    public int countFinishedContests() {
        List<Contest> finishedContests = contestRepository.countFinishedContests();
        if (finishedContests.isEmpty()) {
            return 0;
        } else return finishedContests.size();
    }
}