package com.example.photocontest.services;

import com.example.photocontest.models.Phase;
import com.example.photocontest.repositories.contracts.PhaseRepository;
import com.example.photocontest.services.contracts.PhaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhaseServiceImpl implements PhaseService {

    private final PhaseRepository phaseRepository;

    @Autowired
    public PhaseServiceImpl(PhaseRepository phaseRepository) {
        this.phaseRepository = phaseRepository;
    }

    @Override
    public List<Phase> getAll() {
        return phaseRepository.getAll();
    }

    @Override
    public Phase getById(int id) {
        return phaseRepository.getById(id);
    }
}
