package com.example.photocontest.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ChangePasswordDTO {

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 1, max = 64, message = "Password must be between 8 and 64 symbols.")
    private String oldPassword;

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 1, max = 64, message = "Password must be between 8 and 64 symbols.")
    private String password;

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 1, max = 64, message = "Password must be between 8 and 64 symbols.")
    private String confirmPassword;

    public ChangePasswordDTO() {
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
