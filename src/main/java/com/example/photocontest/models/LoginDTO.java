package com.example.photocontest.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LoginDTO {

    @NotEmpty(message = "Username confirmation can't be empty")
    @Size(min = 4, max = 64, message = "Username must be between 8 and 64 symbols.")
    private String username;

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 1, max = 64, message = "Password must be between 8 and 64 symbols.")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
