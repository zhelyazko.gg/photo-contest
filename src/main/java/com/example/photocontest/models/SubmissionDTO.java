package com.example.photocontest.models;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SubmissionDTO {

    @NotNull(message = "Title cannot be empty.")
    @Size(min = 3, max = 1000, message = "Title must be between 3 and 1000 symbols.")
    private String title;

    @NotNull(message = "Story cannot be empty.")
    @Size(min = 16, max = 8000, message = "Story must be between 16 and 8000 symbols.")
    private String story;

    private MultipartFile multipartFile;

    public SubmissionDTO() {
    }

    public SubmissionDTO(String title, String story, MultipartFile multipartFile) {
        this.title = title;
        this.story = story;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
