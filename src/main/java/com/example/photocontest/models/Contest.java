package com.example.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "phase_id")
    private Phase phase;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "organizer_id")
    private User organizer;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Set<Submission> submissions;

    @JsonIgnore
    @Column(name = "isOpen")
    private boolean open;

    @JsonIgnore
    @Column(name = "deleted")
    private boolean deleted;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;

    @Column(name = "phase_1_time_limit")
    private LocalDateTime phase1;

    @Column(name = "phase_2_time_limit")
    private LocalDateTime phase2;

    @Column(name = "creation")
    private LocalDateTime creation;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Set<User> contestants;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "winners",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "submission_id")
    )
    private List<Submission> winners;

    public Contest() {
    }

    public Contest(int id, String title, Category category, Phase phase, User organizer, boolean open) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.phase = phase;
        this.organizer = organizer;
        this.open = open;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public Set<Submission> getSubmissions() {
        return submissions;
    }

    public void setSubmissions(Set<Submission> submissions) {
        this.submissions = submissions;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public LocalDateTime getPhase1() {
        return phase1;
    }

    public void setPhase1(LocalDateTime phase1) {
        this.phase1 = phase1;
    }

    public LocalDateTime getPhase2() {
        return phase2;
    }

    public void setPhase2(LocalDateTime phase2) {
        this.phase2 = phase2;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public Set<User> getContestants() {
        return contestants;
    }

    public void setContestants(Set<User> contestants) {
        this.contestants = contestants;
    }

    public List<Submission> getWinners() {
        return winners;
    }

    public void setWinners(List<Submission> winners) {
        this.winners = winners;
    }

    public LocalDateTime getTimeLimit() {
        if (LocalDateTime.now().isBefore(getPhase1())) {
            return getPhase1();
        } else if (LocalDateTime.now().isAfter(getPhase1()) && LocalDateTime.now().isBefore(getPhase2())) {
            return getPhase2();
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Contest)) return false;
        Contest contest = (Contest) o;
        return id == contest.id && Objects.equals(title, contest.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }
}
