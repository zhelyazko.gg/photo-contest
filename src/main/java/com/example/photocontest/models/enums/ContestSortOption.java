package com.example.photocontest.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum ContestSortOption {
    // title category phase
    TITLE_ASC("Title, Ascending", " order by title "),
    TITLE_DESC("Title, Descending", " order by title desc "),
    CATEGORY_ASC("Category, Ascending", " order by category "),
    CATEGORY_DESC("Category, Descending", " order by category desc");

    private final String preview;
    private final String query;

    private static final Map<String, ContestSortOption> BY_PREVIEW = new HashMap<>();

    static {
        for (ContestSortOption option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    ContestSortOption(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static ContestSortOption valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
