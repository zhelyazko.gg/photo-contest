package com.example.photocontest.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserSortOption {
    //preview
    //query
    USERNAME_ASC("Username, Ascending", " order by username "),
    USERNAME_DESC("Username, Descending", " order by username desc "),
    FIRSTNAME_ASC("First name, Ascending", " order by first_name "),
    FIRSTNAME_DESC("First name, Descending", " order by first_name desc "),
    EMAIL_ASC("Last name, Ascending", " order by last_name  "),
    EMAIL_DESC("Last name, Descending", " order by last_name desc ");


    private final String preview;
    private final String query;

    private static final Map<String, UserSortOption> BY_PREVIEW = new HashMap<>();

    static {
        for (UserSortOption option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    UserSortOption(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static UserSortOption valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
