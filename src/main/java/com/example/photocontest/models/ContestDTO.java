package com.example.photocontest.models;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ContestDTO {

    @NotNull(message = "Title can not be empty.")
    @Size(min = 1, max = 1000, message = "Title must be between 1 and 1000 symbols.")
    private String title;

    @Positive(message = "Category ID should be positive or zero")
    private int category_id;

    @NotNull
    private MultipartFile multipartFile;

    @NotNull
//    @Size(min = 1, max = 30, message = "Phase 1 must be between 1 dan and 1 month.")
    private int phase1;

    @NotNull
//    @Size(min = 1, max = 24, message = "Phase 2 must be between 1 hour and 24 hours")
    private int phase2;

//    private boolean isOpen;


    public ContestDTO() {
    }

    public ContestDTO(String title, int category_id, boolean isOpen, MultipartFile multipartFile) { //, boolean isOpen
        this.title = title;
        this.category_id = category_id;
        this.multipartFile = multipartFile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

//    public boolean isOpen() {
//        return isOpen;
//    }
//
//    public void setOpen(boolean open) {
//        isOpen = open;
//    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public int getPhase1() {
        return phase1;
    }

    public void setPhase1(int phase1) {
        this.phase1 = phase1;
    }

    public int getPhase2() {
        return phase2;
    }

    public void setPhase2(int phase2) {
        this.phase2 = phase2;
    }

}
