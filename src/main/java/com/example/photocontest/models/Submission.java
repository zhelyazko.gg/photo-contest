package com.example.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "submissions")
public class Submission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User participant;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "contest_id")
    private Contest contest_id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "photo_id")
    private Photo photo;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "submission_id")
    private Set<SubmissionScoring> scoring;

    @Column(name = "points")
    private int points;

//    @JsonIgnore
//    @JoinTable(
//            name = "winners",
//            joinColumns = @JoinColumn(name = "submission_id"),
//            inverseJoinColumns = @JoinColumn(name = "place")
//    )
//    private int winnerPlace;

    public Submission() {
    }

    public Submission(int id, User participant, Contest contest_id, Photo photo, String title, String story) {
        this.id = id;
        this.participant = participant;
        this.contest_id = contest_id;
        this.photo = photo;
        this.title = title;
        this.story = story;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getParticipant() {
        return participant;
    }

    public void setParticipant(User participant) {
        this.participant = participant;
    }

    public Contest getContest_id() {
        return contest_id;
    }

    public void setContest_id(Contest contest_id) {
        this.contest_id = contest_id;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Set<SubmissionScoring> getScoring() {
        return scoring;
    }

    public void setScorings(Set<SubmissionScoring> scoring) {
        this.scoring = scoring;
    }

//    public int getWinnerPlace() {
//        return winnerPlace;
//    }
//
//    public void setWinnerPlace(int winnerPlace) {
//        this.winnerPlace = winnerPlace;
//    }
}
