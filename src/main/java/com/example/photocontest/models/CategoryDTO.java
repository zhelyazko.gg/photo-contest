package com.example.photocontest.models;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryDTO {


    @NotNull
    @Size(min = 1, max = 64, message = "name must be between 1 and 64 symbols.")
    private String name;

    @NotNull
    private MultipartFile multipartFile;


    public CategoryDTO() {
    }

    public CategoryDTO(String name, MultipartFile multipartFile) {
        this.name = name;
        this.multipartFile = multipartFile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
