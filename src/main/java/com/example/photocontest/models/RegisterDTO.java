package com.example.photocontest.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegisterDTO extends LoginDTO {

    @NotEmpty(message = "First name can't be empty")
    @Size(min = 4, max = 32, message = "Name must be between 4 and 32 symbols.")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    @Size(min = 4, max = 32, message = "Name must be between 4 and 32 symbols.")
    private String lastName;

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 8, max = 64, message = "Password must be between 8 and 64 symbols.")
    private String passwordConfirm;

    @NotEmpty(message = "Email can't be empty")
    @Pattern(regexp = "^(.+)@(.+)$", message = "Email should contain @")
    private String email;

    public RegisterDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
