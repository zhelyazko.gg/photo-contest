package com.example.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "phases")
public class Phase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phase_id")
    private int id;

    @Column(name = "phase")
    private String phase;

    public Phase() {
    }

    public Phase(int id, String phase) {
        this.id = id;
        this.phase = phase;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }
}
