package com.example.photocontest.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SubmissionScoringDTO {

    @NotNull(message = "Rating cannot be empty.")
    private int rating_id;

    @NotNull(message = "Content can not be empty.")
    @Size(min = 1, max = 8000, message = "Content must be between 1 and 8000 symbols.")
    private String comment;

    public SubmissionScoringDTO() {
    }

    public SubmissionScoringDTO(int rating, String comment) {
        this.rating_id = rating;
        this.comment = comment;
    }

    public int getRating_id() {
        return rating_id;
    }

    public void setRating_id(int rating_id) {
        this.rating_id = rating_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
