package com.example.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "submission_scoring")
public class SubmissionScoring {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "submission_id")
    private Submission submission;

    @OneToOne
    @JoinColumn(name = "rating_id")
    private Rating rating;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "comment")
    private String comment;

    public SubmissionScoring() {
    }

    public SubmissionScoring(int id, Submission submission, User user) {
        this.id = id;
        this.submission = submission;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Submission getSubmission() {
        return submission;
    }

    public void setSubmission(Submission submission) {
        this.submission = submission;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
