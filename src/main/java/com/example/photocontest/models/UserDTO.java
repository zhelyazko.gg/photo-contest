package com.example.photocontest.models;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTO {

    @NotNull(message = "Name can't be empty.")
    @Size(min = 4, max = 32, message = "Name must be between 4 and 32 symbols.")
    private String firstName;

    @NotNull(message = "Name can't be empty.")
    @Size(min = 4, max = 32, message = "Name must be between 4 and 32 symbols.")
    private String last_name;

    @NotNull(message = "Username can't be empty.")
    @Size(min = 4, max = 32, message = "Username must be between 4 and 32 symbols.")
    private String username;

    @NotNull(message = "Email can't be empty.")
    @Pattern(regexp = "^(.+)@(.+)$", message = "Email should contain @")
    private String email;

    private MultipartFile multipartFile;


    public UserDTO(String firstName, String last_name, String username, String email) {
        this.firstName = firstName;
        this.last_name = last_name;
        this.username = username;
        this.email = email;
    }

    public UserDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
