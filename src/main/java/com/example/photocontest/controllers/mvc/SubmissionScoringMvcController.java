package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.*;
import com.example.photocontest.services.contracts.RatingService;
import com.example.photocontest.services.contracts.SubmissionScoringService;
import com.example.photocontest.services.contracts.SubmissionService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.SubmissionScoringMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/score")
public class SubmissionScoringMvcController {

    private final SubmissionScoringService submissionScoringService;
    private final SubmissionScoringMapper submissionScoringMapper;
    private final AuthenticationHelper authenticationHelper;
    private final SubmissionService submissionService;
    private final RatingService ratingService;


    public SubmissionScoringMvcController(SubmissionScoringService submissionScoringService,
                                          SubmissionScoringMapper submissionScoringMapper, AuthenticationHelper authenticationHelper, SubmissionService submissionService, RatingService ratingService) {
        this.submissionScoringService = submissionScoringService;
        this.submissionScoringMapper = submissionScoringMapper;
        this.authenticationHelper = authenticationHelper;
        this.submissionService = submissionService;
        this.ratingService = ratingService;

    }

    @GetMapping("/{id}/new")
    public String showAddSubmissionsScoringSection(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            model.addAttribute("submission", submissionService.getById(id));
            model.addAttribute("submissionId", id);
            model.addAttribute("submissionScoring", new SubmissionScoringDTO());
            model.addAttribute("ratings", ratingService.getAll());
            model.addAttribute("voted", submissionScoringService.voted(user, id));
            model.addAttribute("currentUser", user);
            return "rate";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/new")
    public String createSubmissionScoring(@PathVariable int id,
                                          @Valid @ModelAttribute("submissionScoring") SubmissionScoringDTO scoringDTO,
                                          BindingResult errors,
                                          Model model,
                                          HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        Submission submission = submissionService.getById(id);
        Contest contest = submission.getContest_id();

        try {
            SubmissionScoring submissionScoring = submissionScoringMapper.createScoring(scoringDTO);
            submissionScoringService.create(submissionScoring, user, submission);
            return String.format("redirect:/contests/%d", contest.getId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateRatingPage(@PathVariable int id,
                                       Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            SubmissionScoring submissionScoring = submissionScoringService.getById(id);
            SubmissionScoringDTO submissionScoringDTO = submissionScoringMapper.toDTO(submissionScoring);
            model.addAttribute("currentUser", user);
            model.addAttribute("ratingId", id);
            model.addAttribute("rating", submissionScoringDTO);
            model.addAttribute("ratings", ratingService.getAll());
            return "rate-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/update")
    public String updateComment(@PathVariable int id,
                                @Valid @ModelAttribute("rating") SubmissionScoringDTO dto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "rate-update";
        }

        try {
            SubmissionScoring submissionScoring = submissionScoringMapper.update(dto, id);
            int subId = submissionScoring.getSubmission().getId();
            int contestId = submissionService.getById(subId).getContest_id().getId();
            submissionScoringService.update(submissionScoring, user, id);
            return String.format("redirect:/contests/%d", contestId);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}