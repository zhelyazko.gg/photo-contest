package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final UserService userService;

    public HomeMvcController(AuthenticationHelper authenticationHelper, ContestService contestService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticateID")
    public int getId(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session).getId();
        } catch (AuthenticationFailureException e) {
            return 0;
        }
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("countUsers", userService.countUsers());
        model.addAttribute("countOrganizers", userService.countOrganizers());
        model.addAttribute("countContests", contestService.countContests());
        model.addAttribute("activeContests", contestService.countOpenContests());
        model.addAttribute("finishedContests", contestService.countFinishedContests());
        model.addAttribute("mostParticipatory", contestService.mostParticipatoryContest());
        model.addAttribute("newestContests", contestService.newestContests());
        return "index";
    }

}
