package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.*;
import com.example.photocontest.models.enums.ContestSortOption;
import com.example.photocontest.services.contracts.*;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.ContestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {

    public static final int PHASE_FINISH_ID = 3;
    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final CategoryService categoryService;
    private final ContestMapper contestMapper;
    private final SubmissionService submissionService;
    private final UserService userService;
    private final PhaseService phaseService;


    @Autowired
    public ContestMvcController(ContestService contestService, AuthenticationHelper authenticationHelper,
                                CategoryService categoryService, ContestMapper contestMapper,
                                SubmissionService submissionService, UserService userService,
                                PhaseService phaseService) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.categoryService = categoryService;
        this.contestMapper = contestMapper;
        this.submissionService = submissionService;
        this.userService = userService;
        this.phaseService = phaseService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.isOrganizer()) {
            return session.getAttribute("currentUser") != null;
        }
        return user.isOrganizer();
    }

    @ModelAttribute("authenticateID")
    public int getId(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session).getId();
        } catch (AuthenticationFailureException e) {
            return 0;
        }
    }

    @ModelAttribute("sortContestOptions")
    public ContestSortOption[] populateSortOptions() {
        return ContestSortOption.values();
    }

    @PostMapping("/search")
    public String searchContest(@ModelAttribute("searchContestDTO") SearchContestDTO searchContestDTO, Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        var searched = contestService.search(
                Optional.ofNullable(searchContestDTO.getTitle().isBlank() ? null : searchContestDTO.getTitle()),
                Optional.ofNullable(searchContestDTO.getCategory() == null ? null : searchContestDTO.getCategory()),
                Optional.ofNullable(searchContestDTO.getPhase() == null ? null : searchContestDTO.getPhase()),
                Optional.ofNullable(searchContestDTO.getSort() == null ? null : ContestSortOption.valueOfPreview
                        (searchContestDTO.getSort()))
        );
        model.addAttribute("contests", searched);
        model.addAttribute("currentUser", user);
        return "contests";
    }

    @GetMapping()
    public String getAll(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        contestService.setPhase();
        model.addAttribute("currentUser", user);
        model.addAttribute("contests", contestService.getAll());
        model.addAttribute("openContests", contestService.getPhase1Contests(user));
        model.addAttribute("searchContestDTO", new SearchContestDTO());
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("phases", phaseService.getAll());
        return "contests";
    }

    @GetMapping("/{id}")
    public String showContest(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(id);
            model.addAttribute("contest", contest);
            model.addAttribute("currentUser", user);
            model.addAttribute("participate", submissionService.participate(user, id));
            model.addAttribute("submission", new SubmissionDTO());
            model.addAttribute("usersJury", userService.getUsersForJury());
            if (contest.getPhase().getId() != PHASE_FINISH_ID) {
                return "contest";
            } else {
                return "contest-finish";
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/new")
    public String showNewContestPage(Model model,
                                     HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contest", new ContestDTO());
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("users", userService.getUsers());
        return "contests-new";
    }

    @PostMapping("/new")
    public String createContest(@Valid @ModelAttribute("contest") ContestDTO contestDTO,
                                BindingResult errors,
                                Model model,
                                HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "contests-new";
        }

        try {
            Contest contest = contestMapper.createContest(contestDTO);
            contestService.create(contest, user, contestDTO.getMultipartFile());
            model.addAttribute("fileImage", contestDTO.getMultipartFile());

            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateContest(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Contest contest = contestService.getById(id);
            ContestDTO contestDTO = contestMapper.toDto(contest);
            model.addAttribute("contestId", id);
            model.addAttribute("contest", contestDTO);
            model.addAttribute("categories", categoryService.getAll());
            return "contest-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/update")
    public String updatePost(@PathVariable int id,
                             @Valid @ModelAttribute("contest") ContestDTO contestDTO,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "contest-update";
        }

        try {
            Contest contest = contestMapper.updateContest(contestDTO, id);
            contestService.update(contest, user, contestDTO.getMultipartFile());

            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteContest(@PathVariable int id,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            contestService.delete(id, user);
            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/add-jury/{userId}")
    public String addJury(@PathVariable int id,
                          @PathVariable int userId,
                          HttpSession session,
                          Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(id);
            User user1 = userService.getById(userId);
            contestService.setJury(user1, contest);
            return String.format("redirect:/contests/%d", id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
