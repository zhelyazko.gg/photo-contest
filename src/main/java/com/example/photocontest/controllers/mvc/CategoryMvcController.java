package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Category;
import com.example.photocontest.models.CategoryDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.CategoryService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("categories")
public class CategoryMvcController {

    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryMvcController(CategoryService categoryService, CategoryMapper categoryMapper,
                                 AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/new")
    public String showNewCategory(Model model,
                                  HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("category", new CategoryDTO());
        return "categories-new";
    }

    @PostMapping("/new")
    public String createCategory(@Valid @ModelAttribute("category") CategoryDTO categoryDTO,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "categories-new";
        }

        try {
            Category category = categoryMapper.createCategory(categoryDTO);
            categoryService.create(category, user, categoryDTO.getMultipartFile());
            model.addAttribute("imageFile", categoryDTO.getMultipartFile());

            return "redirect:/contests/new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
