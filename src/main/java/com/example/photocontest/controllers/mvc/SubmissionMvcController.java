package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.SubmissionService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.SubmissionMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/submissions")
public class SubmissionMvcController {

    private final SubmissionService submissionService;
    private final AuthenticationHelper authenticationHelper;
    private final SubmissionMapper submissionMapper;

    public SubmissionMvcController(SubmissionService submissionService,
                                   AuthenticationHelper authenticationHelper,
                                   SubmissionMapper submissionMapper) {
        this.submissionService = submissionService;
        this.authenticationHelper = authenticationHelper;
        this.submissionMapper = submissionMapper;
    }


    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticateID")
    public int getId(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session).getId();
        } catch (AuthenticationFailureException e) {
            return 0;
        }
    }

    @GetMapping
    public String showAllSubmissions(Model model) {
        Contest contest = new Contest();
        model.addAttribute("submissions", submissionService.getAll());
        return String.format("redirect:/contests/%d", contest.getId());
    }

    @GetMapping("/{id}")
    public String showSubmission(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Submission submission = submissionService.getById(id);
            model.addAttribute("submission", submission);
            return "contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/{id}/new")
    public String showAddSubmissionsSection(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            model.addAttribute("contestId", id);
            model.addAttribute("submission", new SubmissionDTO());
            return "contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{contestId}/new")
    public String createSubmission(@PathVariable int contestId,
                                   @Valid @ModelAttribute("submission") SubmissionDTO submissionDTO,
                                   BindingResult errors,
                                   Model model,
                                   HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return String.format("redirect:/contests/%d", contestId);
        }

        try {
            Submission submission = submissionMapper.createSubmission(submissionDTO);
            submissionService.create(submission, user, contestId, submissionDTO.getMultipartFile());
            model.addAttribute("fileImage", submissionDTO.getMultipartFile());
            return String.format("redirect:/contests/%d", contestId);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteSubmission(@PathVariable int id,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Submission submission = submissionService.getById(id);
            Contest contest = submission.getContest_id();
            submissionService.delete(id, user);
            return String.format("redirect:/contests/%d", contest.getId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
