package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.ChangePasswordDTO;
import com.example.photocontest.models.SearchUserDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserDTO;
import com.example.photocontest.models.enums.UserSortOption;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.RankService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final RankService rankService;
    private final UserMapper userMapper;
    private final PhotoService photoService;

    @Autowired
    public UserMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper,
                             RankService rankService,
                             UserMapper userMapper, PhotoService photoService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.rankService = rankService;
        this.photoService = photoService;
        this.userMapper = userMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticateID")
    public int getId(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session).getId();
        } catch (AuthenticationFailureException e) {
            return 0;
        }
    }

    @GetMapping
    public String getAll(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("users", userService.getUsers());
        model.addAttribute("searchUserDTO", new SearchUserDTO());
        return "users";
    }


    @ModelAttribute("sortUserOptions")
    public UserSortOption[] populateSortOptions() {
        return UserSortOption.values();
    }

    @PostMapping("/search")
    public String searchUsers(@ModelAttribute("searchUserDTO") SearchUserDTO searchUserDTO, Model model) {

        var searched = userService.search(
                Optional.ofNullable(searchUserDTO.getUsername().isBlank() ? null : searchUserDTO.getUsername()),
                Optional.ofNullable(searchUserDTO.getFirst_name().isBlank() ? null : searchUserDTO.getFirst_name()),
                Optional.ofNullable(searchUserDTO.getLast_name().isBlank() ? null : searchUserDTO.getLast_name()),
                Optional.ofNullable(searchUserDTO.getSort() == null ? null : UserSortOption.valueOfPreview(searchUserDTO.getSort()))

        );
        model.addAttribute("users", searched);
        return "users";
    }

    @GetMapping("/{id}")
    public String showUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        User user1;
        try {
            user = userService.getById(id);
            user1 = authenticationHelper.tryGetUser(session);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("user", user);
        model.addAttribute("photo", photoService.getById(user.getPhoto().getPhotoId()));
        model.addAttribute("currentUser", user1);
        model.addAttribute("rank", rankService.getById(user.getRank_id()));
        return "user";
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model,
                                   HttpSession httpSession) {
        User authenticate;
        try {
            authenticate = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User user = userService.getById(id);
            UserDTO userDTO = userMapper.toDTO(user);
            model.addAttribute("userId", id);
            model.addAttribute("user", userDTO);
            model.addAttribute("authenticate", authenticate);
            model.addAttribute("photo", user.getPhoto());
            model.addAttribute("fileImage", userDTO.getMultipartFile());

            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UserDTO userDTO,
                             @ModelAttribute("authenticate") User authenticate,
                             BindingResult errors,
                             Model model) {
        if (errors.hasErrors()) {
            return "user-update";
        }
        try {
            User newUser = userMapper.updateUser(userDTO, id);
            userService.update(newUser, authenticate, userDTO.getMultipartFile());
            model.addAttribute("fileImage", userDTO.getMultipartFile());
            return String.format("redirect:/users/%d", id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/changePassword")
    public String showChangePasswordPage(@PathVariable int id, Model model, HttpSession httpSession) {
        User authenticate;
        try {
            authenticate = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User user = userService.getById(id);
            model.addAttribute("password", new ChangePasswordDTO());
            model.addAttribute("authenticate", authenticate);
            model.addAttribute("userId", id);
            return "change_password";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/changePassword")
    public String changePassword(@PathVariable int id,
                                 @Valid @ModelAttribute("password") ChangePasswordDTO dto,
                                 @ModelAttribute("authenticate") User authenticate,
                                 BindingResult errors,
                                 Model model) {
        if (errors.hasErrors()) {
            return "user-update";
        }
        if (!userService.getById(id).getPassword().equals(dto.getOldPassword())) {
            throw new UnauthorizedOperationException("Old password didn't match!");
        }

        try {
            User newUser = userMapper.changePassword(dto, id);
            userService.update(newUser, authenticate);
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/promote")
    public String promoteUserRank(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            userService.promote(user, id);
            return String.format("redirect:/users/%d", id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/demote")
    public String demoteUserRank(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            userService.demote(user, id);
            return String.format("redirect:/users/%d", id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}

