package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Category;
import com.example.photocontest.models.CategoryDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.CategoryService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryMapper categoryMapper,
                              AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping("/new")
    public Category createCategory(@RequestHeader HttpHeaders headers, @Valid CategoryDTO categoryDTO) {
        User user = authenticationHelper.tryGetUser(headers);
        Category category = categoryMapper.createCategory(categoryDTO);
        try {
            categoryService.create(category, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return category;
    }
}
