package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.ContestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestMapper contestMapper;

    @Autowired
    public ContestController(ContestService contestService, AuthenticationHelper authenticationHelper, ContestMapper contestMapper) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.contestMapper = contestMapper;
    }

    @GetMapping
    public List<Contest> getContests() {
        return contestService.getAll();
    }

    @GetMapping("/{id}")
    public Contest findContestById(@PathVariable int id) {
        try {
            return contestService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public Contest create(@RequestHeader HttpHeaders headers,
                          @RequestParam("Body") ContestDTO contestDTO,
                          @RequestParam("File") MultipartFile file) {
        User user = authenticationHelper.tryGetUser(headers);
        Contest contest = contestMapper.createContest(contestDTO);
        try {
            contestService.create(contest, user, file);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return contest;
    }

    @PutMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ContestDTO contestDTO) {
        User user = authenticationHelper.tryGetUser(headers);
        Contest newContest = contestMapper.updateContest(contestDTO, id);
        try {
            contestService.update(newContest, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return newContest;
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            contestService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
