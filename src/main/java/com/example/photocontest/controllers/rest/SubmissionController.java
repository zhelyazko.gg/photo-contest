package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.SubmissionService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.mappers.SubmissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/submissions")
public class SubmissionController {

    private final SubmissionService submissionService;
    private final SubmissionMapper submissionMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public SubmissionController(SubmissionService submissionService, SubmissionMapper submissionMapper,
                                AuthenticationHelper authenticationHelper) {
        this.submissionService = submissionService;
        this.submissionMapper = submissionMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Submission> getAll() {
        return submissionService.getAll();
    }

    @GetMapping("/{id}")
    public Submission findSubmissionById(@PathVariable int id) {
        try {
            return submissionService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("{contestId}")
    public Submission create(@RequestHeader HttpHeaders headers, @PathVariable int contestId,
                             @Valid @RequestBody SubmissionDTO submissionDTO) {
        Submission submission = submissionMapper.createSubmission(submissionDTO);
        User user = authenticationHelper.tryGetUser(headers);
        try {
            submissionService.create(submission, user, contestId, submissionDTO.getMultipartFile());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return submission;
    }
}
