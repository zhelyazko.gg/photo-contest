package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.ChangePasswordDTO;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserDTO;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.helpers.AuthenticationHelper;
import com.example.photocontest.utils.helpers.AuthorizationHelpers;
import com.example.photocontest.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService service, UserMapper mapper, AuthenticationHelper authenticationHelper) {
        this.userService = service;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;

    }

    @GetMapping
    public List<User> getAll() {
        return userService.getUsers();
    }

    @PostMapping("/new")
    public ResponseEntity<Object> fileUpload(@RequestParam("File") MultipartFile file) throws IOException {
        Photo photo = new Photo();
        return new ResponseEntity<Object>("The File Uploaded Successfully.", HttpStatus.OK);
    }

    @PutMapping("/{id}/update")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDTO userDTO) {
        User authenticate = authenticationHelper.tryGetUser(headers);
        AuthorizationHelpers.authorizeSameUser(authenticate, findUserById(id));
        User newUser = mapper.updateUser(userDTO, id);

        try {
            userService.update(newUser, authenticate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return newUser;
    }

    @GetMapping("/{id}")
    public User findUserById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/promote")
    public String promote(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        try {
            return userService.promote(user, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/demote")
    public String demote(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        try {
            return userService.demote(user, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/changePassword")
    public User changePassword(@RequestHeader HttpHeaders headers,
                               @PathVariable int id,
                               @Valid @RequestBody UserDTO userDTO,
                               @Valid ChangePasswordDTO changePasswordDTO) {
        User authenticate = authenticationHelper.tryGetUser(headers);
        AuthorizationHelpers.authorizeSameUser(authenticate, findUserById(id));
        User newUser = mapper.updateUser(userDTO, id);
        if (!userService.getById(id).getPassword().equals(changePasswordDTO.getOldPassword())) {
            throw new UnauthorizedOperationException("Old password didn't match");
        }
        try {
            userService.update(newUser, authenticate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return newUser;
    }
}
