package com.example.photocontest.utils.helpers;


import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.User;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationHelpers {

    private static final String YOU_ARE_NOT_ADMIN_ERROR = "Only an admin can make this changes.";
    private static final String YOU_ARE_NOT_ADMIN_OR_CREATOR_ERROR = "Only the creator or admin can make this changes.";
    private static final String UPDATE_USER_ERROR_MESSAGE = "You are not authorized to perform this action.";
    public static final int RANK_ORGANIZER_ID = 1;

    public static void authorizeOrganizer(User user) {
        if (user.getRank_id() != RANK_ORGANIZER_ID) {
            throw new UnauthorizedOperationException(YOU_ARE_NOT_ADMIN_ERROR);
        }
    }

    public static void authorizeSameUser(User user1, User user) {
        if (!user1.getUsername().equals(user.getUsername())) {
            throw new UnauthorizedOperationException(UPDATE_USER_ERROR_MESSAGE);
        }
    }
}
