package com.example.photocontest.utils.mappers;

import com.example.photocontest.models.ChangePasswordDTO;
import com.example.photocontest.models.RegisterDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserDTO;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class UserMapper {

    public static final int JUNKIE_RANK = 2;
    public static final int STARTING_POINTS = 0;
    public static final int DEFAULT_PHOTO_ID = 1;
    private final UserService userService;
    private final PhotoService photoService;

    @Autowired
    public UserMapper(UserService userService, PhotoService photoService) {
        this.userService = userService;
        this.photoService = photoService;
    }

    public User updateUser(UserDTO userDTO, int id) {
        User user = userService.getById(id);
        dtoToObject(userDTO, user);
        return user;
    }

    public User changePassword(ChangePasswordDTO dto, int id) {
        User user = userService.getById(id);
        user.setPassword(dto.getPassword());
        return user;
    }

    public UserDTO toDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(user.getFirst_name());
        userDTO.setLast_name(user.getLast_name());
        userDTO.setUsername(user.getUsername());
        userDTO.setEmail(user.getEmail());
        return userDTO;
    }

    public void dtoToObject(UserDTO userDTO, User user) {
        user.setFirst_name(userDTO.getFirstName());
        user.setLast_name(userDTO.getLast_name());
        user.setUsername(userDTO.getUsername());
        user.setPhoto(photoService.getById(1));
        user.setEmail(userDTO.getEmail());
    }

    public User fromDto(RegisterDTO registerDto) {
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setFirst_name(registerDto.getFirstName());
        user.setLast_name(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setRank_id(JUNKIE_RANK);
        user.setPoints(STARTING_POINTS);
        user.setTimestamp(LocalDateTime.now());
        user.setBlocked(false);
        user.setPhoto(photoService.getById(DEFAULT_PHOTO_ID));
        return user;
    }

}
