package com.example.photocontest.utils.mappers;

import com.example.photocontest.models.Category;
import com.example.photocontest.models.CategoryDTO;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {


    public Category createCategory(CategoryDTO categoryDTO) {
        Category category = new Category();
        dtoToObject(categoryDTO, category);
        return category;
    }

    public void dtoToObject(CategoryDTO categoryDTO, Category category) {
        category.setName(categoryDTO.getName());
    }

}
