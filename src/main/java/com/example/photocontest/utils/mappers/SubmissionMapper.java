package com.example.photocontest.utils.mappers;

import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionDTO;
import org.springframework.stereotype.Component;

@Component
public class SubmissionMapper {

    public Submission createSubmission(SubmissionDTO submissionDTO) {
        Submission submission = new Submission();
        dtoToObject(submissionDTO, submission);
//        submission.setTimestamp(LocalDate.now());
        submission.setPoints(0);
        return submission;
    }

    public Submission dtoToObject(SubmissionDTO submissionDTO, Submission submission) {
        submission.setTitle(submissionDTO.getTitle());
        submission.setStory(submissionDTO.getStory());
        return submission;
    }
}
