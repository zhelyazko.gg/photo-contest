package com.example.photocontest.utils.mappers;

import com.example.photocontest.models.Category;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestDTO;
import com.example.photocontest.repositories.contracts.CategoryRepository;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.PhaseService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ContestMapper {

    public static final int OPEN_CONTEST = 1;
    private final CategoryRepository categoryRepository;
    private final PhaseService phaseService;
    private final ContestService contestService;
    private final UserService userService;

    @Autowired
    public ContestMapper(CategoryRepository categoryRepository, PhaseService phaseService, ContestService contestService, UserService userService) {
        this.categoryRepository = categoryRepository;
        this.phaseService = phaseService;
        this.contestService = contestService;
        this.userService = userService;
    }

    public Contest createContest(ContestDTO contestDTO) {
        Contest contest = new Contest();
        dtoToObject(contestDTO, contest);
        contest.setCreation(LocalDateTime.now());
        contest.setPhase1(contest.getCreation().plusDays(contestDTO.getPhase1()));
        contest.setPhase2(contest.getPhase1().plusHours(contestDTO.getPhase2()));
        contest.setDeleted(false);
        return contest;
    }

    public ContestDTO toDto(Contest contest) {
        ContestDTO contestDTO = new ContestDTO();
        contestDTO.setTitle(contest.getTitle());
        contestDTO.setCategory_id(contest.getCategory().getId());
        contestDTO.setPhase1(contest.getPhase1().getDayOfYear() - LocalDateTime.now().getDayOfYear());
        contestDTO.setPhase2(contest.getPhase2().getHour() - LocalDateTime.now().getHour());
        return contestDTO;
    }

    public Contest updateContest(ContestDTO contestDTO, int id) {
        Contest contest = contestService.getById(id);
        dtoToObject(contestDTO, contest);
        contest.setCreation(LocalDateTime.now());
        contest.setPhase1(contest.getCreation().plusDays(contestDTO.getPhase1()));
        contest.setPhase2(contest.getPhase1().plusHours(contestDTO.getPhase2()));
        return contest;
    }

    private void dtoToObject(ContestDTO contestDTO, Contest contest) {
        Category category = categoryRepository.getById(contestDTO.getCategory_id());
        contest.setTitle(contestDTO.getTitle());
        contest.setCategory(category);
        contest.setPhase(phaseService.getById(OPEN_CONTEST));
    }

}
