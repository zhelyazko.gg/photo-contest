package com.example.photocontest.utils.mappers;

import com.example.photocontest.models.SubmissionScoring;
import com.example.photocontest.models.SubmissionScoringDTO;
import com.example.photocontest.services.contracts.RatingService;
import com.example.photocontest.services.contracts.SubmissionScoringService;
import org.springframework.stereotype.Component;

@Component
public class SubmissionScoringMapper {

    private final RatingService ratingService;
    private final SubmissionScoringService service;

    public SubmissionScoringMapper(RatingService ratingService, SubmissionScoringService service) {
        this.ratingService = ratingService;
        this.service = service;
    }

    public SubmissionScoring createScoring(SubmissionScoringDTO submissionScoringDTO) {
        SubmissionScoring submissionScoring = new SubmissionScoring();
        dtoToObject(submissionScoringDTO, submissionScoring);
        return submissionScoring;
    }

    public SubmissionScoring dtoToObject(SubmissionScoringDTO submissionScoringDTO, SubmissionScoring submissionScoring) {
        submissionScoring.setRating(ratingService.getById(submissionScoringDTO.getRating_id()));
        submissionScoring.setComment(submissionScoringDTO.getComment());
        return submissionScoring;
    }


    public SubmissionScoringDTO toDTO(SubmissionScoring submissionScoring) {
        SubmissionScoringDTO dto = new SubmissionScoringDTO();
        dto.setComment(submissionScoring.getComment());
        dto.setRating_id(submissionScoring.getRating().getId());
        return dto;
    }

    public SubmissionScoring update(SubmissionScoringDTO dto, int id) {
        SubmissionScoring submissionScoring = service.getById(id);
        submissionScoring.setComment(dto.getComment());
        submissionScoring.setRating(ratingService.getById(dto.getRating_id()));
        return submissionScoring;
    }
}
