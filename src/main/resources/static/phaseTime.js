// Set the date we're counting down to
let demo = document.getElementsByClassName("demo");
// Update the count down every 1 second
const x = setInterval(function () {
    // Get today's date and time
    const now = new Date().getTime();

    for (let i = 0; i < demo.length; i++) {
        const finalDateTime = demo[i].dataset.endtime;
        const countDownDate = new Date(finalDateTime).getTime();
        const distance = countDownDate - now;

        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);

        demo[i].innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";
        if (distance < 0) {
            demo = document.getElementsByClassName("demo");
            if (demo === null) {
                clearInterval(x)
                demo[i].innerHTML = "Finished";
            }
        }
    }
}, 1000);