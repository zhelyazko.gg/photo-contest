package com.example.photocontest;

import com.example.photocontest.models.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class Helpers {

    public Helpers(){
    }

    public static User createMockUser() {
        return createMockUser(3);
    }

    public static User createMockOrganizer() {
        return createMockUser(1);
    }

    public static User createMockUser1() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setEmail("mockUser@user.com");
        mockUser.setUsername("MockUsername1");
        mockUser.setLast_name("MockLastName1");
        mockUser.setPassword("MockPassword1");
        mockUser.setFirst_name("MockFirstName1");
        mockUser.setRank_id(4);
        return mockUser;
    }

    private static User createMockUser(int rankId) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLast_name("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirst_name("MockFirstName");
        mockUser.setRank_id(rankId);
        return mockUser;
    }

    public static Contest createMockContest() throws IOException {
        var mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("TestContest");
        mockContest.setOrganizer(createMockOrganizer());
        mockContest.setCategory(createMockCategory());
        mockContest.setPhoto(createMockPhoto());
        mockContest.setPhase(createMockPhase());
        mockContest.setPhase1(LocalDateTime.now().plusDays(1));
        mockContest.setPhase2(LocalDateTime.now().plusDays(1).plusHours(1));
        mockContest.setCreation(LocalDateTime.now());
        return mockContest;
    }

    public static Submission createMockSubmission() throws IOException {
        var mockSubmission = new Submission();
        mockSubmission.setId(1);
        mockSubmission.setPhoto(createMockPhoto());
        mockSubmission.setTitle("TestSubmission");
        mockSubmission.setStory("TestSubmissionStory");
        mockSubmission.setPoints(0);
        return mockSubmission;
    }

    public static MockMultipartFile createMockMultipartFile() throws IOException {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.jpg",
                "multipart/form-data", "Spring Framework".getBytes());
        return multipartFile;
    }

    public static Phase createMockPhase() {
        var mockPhase = new Phase();
        mockPhase.setId(1);
        mockPhase.setPhase("Phase 1");
        return mockPhase;
    }

    public static UserRank createMockRank() {
        var mockRank = new UserRank();
        mockRank.setId(1);
        mockRank.setRole("Junkie");
        return mockRank;
    }

    public static Rating createMockRating() {
        var mockRating = new Rating();
        mockRating.setId(1);
        mockRating.setScore(1);
        return mockRating;
    }


    public static Photo createMockPhoto() throws IOException {
        var mockPhoto = new Photo();
        mockPhoto.setPhotoId(1);
        mockPhoto.setUser(createMockOrganizer());
        mockPhoto.setFileName("test.jpg");
        Path currentPath = Paths.get(".");
        Path absolutePath = currentPath.toAbsolutePath();
        mockPhoto.setPath(absolutePath + "/src/main/resources/static/img/");
        return mockPhoto;
    }

    public static SubmissionScoring createMockSubmissionScoring(){
        var mockSubmissionScoring = new SubmissionScoring();
        mockSubmissionScoring.setId(1);
        mockSubmissionScoring.setComment("MockComment");
        mockSubmissionScoring.setRating(createMockRating());
        return mockSubmissionScoring;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("Art");
        return mockCategory;
    }

    public static ContestDTO createValidContestDTO() throws IOException {
        FileInputStream fis = new FileInputStream("/src/main/resources/static/img/fg1bsy.jpg");
        MockMultipartFile multipartFile = new MockMultipartFile("file", fis);
        ContestDTO dto = new ContestDTO();
        dto.setTitle("TestContest");
        dto.setCategory_id(1);
        dto.setMultipartFile(multipartFile);
        dto.setPhase1(1);
        dto.setPhase2(1);
        return dto;
    }


    /**
     * Accepts an object and returns the stringified object.
     * Useful when you need to pass a body to a HTTP request.
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
