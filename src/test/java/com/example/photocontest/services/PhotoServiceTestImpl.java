package com.example.photocontest.services;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Photo;
import com.example.photocontest.repositories.contracts.PhotoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static com.example.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceTestImpl {

    @Mock
    PhotoRepository mockRepository;

    @InjectMocks
    PhotoServiceImpl service;

    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        Photo mockPhoto = createMockPhoto();
        Mockito.when(mockRepository.getById(mockPhoto.getPhotoId()))
                .thenReturn(mockPhoto);
        // Act
        Photo result = service.getById(mockPhoto.getPhotoId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPhoto.getPhotoId(), result.getPhotoId()),
                () -> Assertions.assertEquals(mockPhoto.getPath(), result.getPath()),
                () -> Assertions.assertEquals(mockPhoto.getFileName(), result.getFileName()),
                () -> Assertions.assertEquals(mockPhoto.getUser(), result.getUser())
        );
    }



//
//    @Test
//    public void shouldSaveUploadedFile() throws Exception {
//
//        Photo mockPhoto = createMockPhoto();
//
//        service.create(createMockMultipartFile(), createMockOrganizer());
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .create(mockPhoto);
//    }


//    @Test
//    public void create_should_callRepository() throws IOException {
//        // Arrange
//        Photo mockPhoto = createMockPhoto();
//
//
//        // Act
//        service.create(createMockMultipartFile(), createMockOrganizer());
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .create(mockPhoto);
//    }
}
