package com.example.photocontest.services;

import com.example.photocontest.models.Category;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Phase;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.CategoryRepository;
import com.example.photocontest.services.contracts.PhotoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTestImpl {

    @Mock
    CategoryRepository repository;

    @Mock
    PhotoService photoService;

    @InjectMocks
    CategoryServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();
        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(repository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);
        // Act
        Category result = service.getById(mockCategory.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName())
        );
    }

    @Test
    public void create_should_callRepository() throws IOException {

        // Arrange
        Category mockCategory = createMockCategory();

        User mockOrganizer = createMockOrganizer();

        // Act
        service.create(mockCategory, createMockOrganizer(), createMockMultipartFile());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .create(mockCategory);
    }

}
