package com.example.photocontest.services;

import com.example.photocontest.models.Category;
import com.example.photocontest.models.UserRank;
import com.example.photocontest.repositories.contracts.RankRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.photocontest.Helpers.createMockCategory;
import static com.example.photocontest.Helpers.createMockRank;

@ExtendWith(MockitoExtension.class)
public class RankServiceTestImpl {

    @Mock
    RankRepository repository;

    @InjectMocks
    RankServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();
        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        UserRank mockRank = createMockRank();
        Mockito.when(repository.getById(mockRank.getId()))
                .thenReturn(mockRank);
        // Act
        UserRank result = service.getById(mockRank.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRank.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRank.getRole(), result.getRole())
        );
    }
}
