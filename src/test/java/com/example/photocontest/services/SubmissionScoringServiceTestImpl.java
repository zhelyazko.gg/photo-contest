package com.example.photocontest.services;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.SubmissionScoring;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.SubmissionScoringRepository;
import com.example.photocontest.services.contracts.SubmissionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static com.example.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class SubmissionScoringServiceTestImpl {

    @Mock
    SubmissionScoringRepository repository;

    @Mock
    SubmissionService submissionService;

    @InjectMocks
    SubmissionScoringServiceImpl service;

//    @Test
//    public void create_should_call_repository_when_called() throws IOException {
//
//        SubmissionScoring mockSubmissionScoring = createMockSubmissionScoring();
//
//        User user = createMockUser();
//
//        var mockSubmission = createMockSubmission();
//
//        service.create(mockSubmissionScoring, user, mockSubmission);
//
//        Mockito.verify(repository, Mockito.times(1)).
//                create(mockSubmissionScoring);
//    }

    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        SubmissionScoring mockScoring = createMockSubmissionScoring();
        Mockito.when(repository.getById(mockScoring.getId()))
                .thenReturn(mockScoring);
        // Act
        SubmissionScoring result = service.getById(mockScoring.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockScoring.getId(), result.getId()),
                () -> Assertions.assertEquals(mockScoring.getSubmission(), result.getSubmission()),
                () -> Assertions.assertEquals(mockScoring.getRating(), result.getRating()),
                () -> Assertions.assertEquals(mockScoring.getComment(), result.getComment()),
                () -> Assertions.assertEquals(mockScoring.getUser(), result.getUser())
        );
    }

}
