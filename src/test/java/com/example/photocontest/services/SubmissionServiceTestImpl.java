package com.example.photocontest.services;

import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Submission;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.SubmissionRepository;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.SubmissionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class SubmissionServiceTestImpl {

    @Mock
    SubmissionRepository mockRepository;

    @Mock
    PhotoService photoService;

    @Mock
    ContestService contestService;

    @InjectMocks
    SubmissionServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }


    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        Submission mockSubmission = createMockSubmission();
        Mockito.when(mockRepository.getById(mockSubmission.getId()))
                .thenReturn(mockSubmission);
        // Act
        Submission result = service.getById(mockSubmission.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockSubmission.getId(), result.getId()),
                () -> Assertions.assertEquals(mockSubmission.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockSubmission.getStory(), result.getStory()),
                () -> Assertions.assertEquals(mockSubmission.getPhoto(), result.getPhoto()),
                () -> Assertions.assertEquals(mockSubmission.getPoints(), result.getPoints())
        );
    }
//
//    @Test
//    public void create_should_callRepository() throws IOException {
//
//        Submission mockSubmission = createMockSubmission();
//
//        Contest mockContest = createMockContest();
//
//
//        // Act
//        service.create(mockSubmission, createMockUser(), mockContest.getId(), createMockMultipartFile());
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .create(mockSubmission);
//    }


    @Test
    void update_should_callRepository_when_contest_is_updated () throws IOException {
        // Arrange
        Submission submission = createMockSubmission();

        // Act
        service.update(submission);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(submission);
    }

//    @Test
//    void delete_should_callRepository_when_contest_is_updated () throws IOException {
//        // Arrange
//        Submission submission = createMockSubmission();
//
//        User mockOrganizer = createMockOrganizer();
//
//        // Act
//        service.delete(submission.getId(), mockOrganizer);
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .update(submission);
//    }

    @Test
    void delete_should_throw_exeption_whne_user_is_not_organizer () throws IOException {

        Submission submission = createMockSubmission();

        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(submission.getId(), mockUser));
    }
}
