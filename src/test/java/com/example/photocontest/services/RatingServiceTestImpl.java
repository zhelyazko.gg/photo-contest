package com.example.photocontest.services;

import com.example.photocontest.models.Rating;
import com.example.photocontest.repositories.contracts.RatingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.photocontest.Helpers.createMockRating;

@ExtendWith(MockitoExtension.class)
public class RatingServiceTestImpl {

    @Mock
    RatingRepository repository;

    @InjectMocks
    RatingServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();
        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        Rating mockRating = createMockRating();
        Mockito.when(repository.getById(mockRating.getId()))
                .thenReturn(mockRating);
        // Act
        Rating result = service.getById(mockRating.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRating.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRating.getScore(), result.getScore())
        );
    }
}
