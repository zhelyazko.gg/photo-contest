package com.example.photocontest.services;

import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.PhotoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.OngoingStubbing;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.photocontest.Helpers.createMockOrganizer;
import static com.example.photocontest.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UserServiceTestImpl {
    @Mock
    UserRepository mockRepository;

    @Mock
    PhotoService photoService;

    @InjectMocks
    UserServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getUsers();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }


    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);
        // Act
        User result = service.getById(mockUser.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPhoto(), result.getPhoto()),
                () -> Assertions.assertEquals(mockUser.getPoints(), result.getPoints()),
                () -> Assertions.assertEquals(mockUser.getFirst_name(), result.getFirst_name()),
                () -> Assertions.assertEquals(mockUser.getLast_name(), result.getLast_name())
                );
    }

    @Test
    public void create_should_callRepository() throws IOException {

        User mockUser = createMockUser();

        // Act
        service.create(mockUser);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    void countUsers_should_callRepository_when_called(){
        Mockito.when(service.countUsers()).thenReturn(Long.valueOf(1));
        Long users = service.countUsers();
        Assertions.assertEquals(users, 1);
    }

    @Test
    void countOrganizers_should_callRepository_when_called (){
        Mockito.when(service.countOrganizers()).thenReturn(1);
        int organizers = service.countOrganizers();
        Assertions.assertEquals(organizers, 1);
    }

    @Test
    void promote_should_throw_exception_when_called_from_user(){
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.promote(createMockUser(), 1));
    }

    @Test
    void demote_should_throw_exception_when_called_from_user(){
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.demote(createMockUser(), 1));
    }

}
