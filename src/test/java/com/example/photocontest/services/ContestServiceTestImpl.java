package com.example.photocontest.services;

import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.services.contracts.PhotoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceTestImpl {

    @Mock
    ContestRepository mockRepository;

    @Mock
    PhotoService photoService;


    @InjectMocks
    ContestServiceImpl service;


    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void get_Phase1_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getPhase1ContestsWhereUserIsNotJury(createMockUser()))
                .thenReturn(new ArrayList<>());

        // Act
        service.getPhase1Contests(createMockUser());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getPhase1ContestsWhereUserIsNotJury(createMockUser());
    }
//
    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        Contest mockContest = createMockContest();
        Mockito.when(mockRepository.getById(mockContest.getId()))
                .thenReturn(mockContest);
        // Act
        Contest result = service.getById(mockContest.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockContest.getId(), result.getId()),
                () -> Assertions.assertEquals(mockContest.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockContest.getCategory(), result.getCategory()),
                () -> Assertions.assertEquals(mockContest.getPhoto(), result.getPhoto()),
                () -> Assertions.assertEquals(mockContest.getPhase(), result.getPhase()),
                () -> Assertions.assertEquals(mockContest.getPhase1(), result.getPhase1()),
                () -> Assertions.assertEquals(mockContest.getPhase2(), result.getPhase2()),
                () -> Assertions.assertEquals(mockContest.getCreation(), result.getCreation()),
                () -> Assertions.assertEquals(mockContest.getOrganizer(), result.getOrganizer())
        );
    }

    @Test
    public void create_should_callRepository() throws IOException {

        // Arrange
        Contest mockContest = createMockContest();

        // Act
        service.create(mockContest, createMockOrganizer(), createMockMultipartFile());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockContest);
    }

    @Test
    public void create_should_throwException_when_userIsNotOrganizer() throws IOException {

        // Arrange
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockContest, createMockUser(), createMockMultipartFile()));
    }

    @Test
    public void update_should_throwException_when_userIsNotOrganizer() throws IOException {
        // Arrange
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockContest, mockUser));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockContest, mockUser, createMockMultipartFile()));
    }

    @Test
    public void delete_should_throwException_when_userIsNotOrganizer() throws IOException {

        // Arrange
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(1, mockUser));
    }

    @Test
    void update_should_callRepository_when_contest_is_updated () throws IOException {
        // Arrange
        Contest mockContest = createMockContest();
        User mockUser = createMockOrganizer();

        // Act
        service.update(mockContest, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockContest);

        service.update(mockContest, mockUser, createMockMultipartFile());

        Mockito.verify(mockRepository, Mockito.times(2))
                .update(mockContest);
    }

    @Test
    void countContests_should_callRepository_when_called () throws IOException {
        Mockito.when(service.countContests()).thenReturn(1);
        int contests = service.countContests();
        Assertions.assertEquals(contests, 1);
    }

//    @Test
//    void setJury_should_set_Jury_when_called() throws IOException {
//        Contest contest = createMockContest();
//        User user = createMockUser();
//        service.setJury(user, contest);
//        Assertions.assertTrue(contest.getJury().contains(user));
//    }

//    @Test
//    void delete_should_update_contest_when_called () throws IOException {
//        // Arrange
//        Contest mockContest = createMockContest();
//        User mockUser = createMockOrganizer();
//
//        // Act
//        service.delete(mockContest.getId(), mockUser);
//
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .update(mockContest);
//
//    }

//    @Test
//    void umostParticipatoryContest_should_callRepository_when_cllaed (){
//        // Arrange
//
//        User mockUser = createMockOrganizer();
//
//        // Act
//        service.update(mockBeer, mockUser);
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .update(mockBeer);
//
//        service.update(mockBeer, mockUser, createMockMultipartFile());
//
//        Mockito.verify(mockRepository, Mockito.times(2))
//                .update(mockBeer);
//
//    }


}
