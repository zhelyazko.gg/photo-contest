package com.example.photocontest.services;

import com.example.photocontest.models.Phase;
import com.example.photocontest.repositories.contracts.PhaseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.photocontest.Helpers.createMockPhase;

@ExtendWith(MockitoExtension.class)
public class PhaseServiceTestImpl {

    @Mock
    PhaseRepository repository;

    @InjectMocks
    PhaseServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }
    @Test
    public void getById_should_returnContest_when_matchExist() throws IOException {
        // Arrange
        Phase mockPhase = createMockPhase();
        Mockito.when(repository.getById(mockPhase.getId()))
                .thenReturn(mockPhase);
        // Act
        Phase result = service.getById(mockPhase.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPhase.getId(), result.getId()),
                () -> Assertions.assertEquals(mockPhase.getPhase(), result.getPhase())
        );
    }


}
