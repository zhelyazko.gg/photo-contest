create or replace table categories
(
    category_id int auto_increment
        primary key,
    name        varchar(64) not null,
    photo_id    int(64)     null
)
    auto_increment = 15;

create or replace table phases
(
    phase_id int auto_increment
        primary key,
    phase    varchar(64) null
)
    auto_increment = 4;

create or replace table photos
(
    photo_id int auto_increment
        primary key,
    path     varchar(280) null,
    name     varchar(280) null,
    user_id  int          not null
)
    auto_increment = 63;

create or replace table rating
(
    id    int auto_increment
        primary key,
    score int not null
)
    auto_increment = 11;

create or replace table user_rank
(
    role_id int auto_increment
        primary key,
    role    varchar(45) null
)
    auto_increment = 6;

create or replace table users
(
    user_id              int auto_increment
        primary key,
    first_name           varchar(32)                            not null,
    last_name            varchar(32)                            not null,
    username             varchar(32)                            not null,
    password             varchar(20)                            not null,
    email                varchar(60)                            not null,
    rank_id              int        default 1                   not null,
    points               int        default 0                   not null,
    creation             datetime   default current_timestamp() null,
    blocked              tinyint(1) default 0                   null,
    photo_id             int        default 2                   null,
    reset_password_token varchar(30)                            null,
    constraint users_username_uindex
        unique (username),
    constraint users_photos_photo_id_fk
        foreign key (photo_id) references photos (photo_id),
    constraint users_role__fk
        foreign key (rank_id) references user_rank (role_id)
)
    auto_increment = 40;

create or replace table contests
(
    contest_id         int auto_increment
        primary key,
    title              varchar(1000)        not null,
    category_id        int                  null,
    phase_id           int        default 1 null,
    organizer_id       int                  null,
    isOpen             tinyint(1) default 0 not null,
    photo_id           int                  not null,
    phase_1_time_limit datetime             not null,
    phase_2_time_limit datetime             null,
    creation           datetime             not null,
    deleted            tinyint(1) default 0 not null,
    constraint contests_title_uindex
        unique (title),
    constraint contests_category__fk
        foreign key (category_id) references categories (category_id),
    constraint contests_phase__fk
        foreign key (phase_id) references phases (phase_id),
    constraint contests_users_user_id_fk
        foreign key (organizer_id) references users (user_id)
)
    auto_increment = 23;

create or replace table jury
(
    contest_id int not null,
    user_id    int not null,
    constraint jury_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint jury_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

alter table photos
    add constraint photos_users_user_id_fk
        foreign key (user_id) references users (user_id);

create or replace table submissions
(
    id         int auto_increment
        primary key,
    user_id    int           null,
    contest_id int           null,
    photo_id   int           null,
    title      varchar(60)   null,
    story      varchar(8000) null,
    points     int default 0 null,
    constraint submissions_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint submissions_photos_photo_id_fk
        foreign key (photo_id) references photos (photo_id),
    constraint submissions_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    auto_increment = 9;

create or replace table submission_scoring
(
    id            int auto_increment
        primary key,
    submission_id int           null,
    rating_id     int default 3 null,
    user_id       int           not null,
    comment       varchar(8000) not null,
    constraint submission_scoring_rating_id_fk
        foreign key (rating_id) references rating (id),
    constraint submission_scoring_submissions_id_fk
        foreign key (submission_id) references submissions (id),
    constraint submission_scoring_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    auto_increment = 6;

create or replace table user_contests
(
    user_id    int null,
    contest_id int null,
    constraint user_contests_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint user_contests_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table winners
(
    contest_id    int not null,
    submission_id int null,
    constraint winners_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint winners_submissions_id_fk
        foreign key (submission_id) references submissions (id)
);

