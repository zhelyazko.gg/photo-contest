-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for photo_contest
CREATE DATABASE IF NOT EXISTS `photo_contest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `photo_contest`;

-- Dumping structure for table photo_contest.categories
CREATE TABLE IF NOT EXISTS `categories` (
    `category_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(64) NOT NULL,
    `photo_id` int(64) DEFAULT NULL,
    PRIMARY KEY (`category_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.categories: ~9 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `name`, `photo_id`) VALUES
                                                                 (1, 'Art of Nature', 1),
                                                                 (8, 'Aquatic Life', 1),
                                                                 (9, 'Winged Life', 1),
                                                                 (10, 'Landscapes, Waterscapes, and Flora', 1),
                                                                 (11, 'Terrestrial Wildlife', 1),
                                                                 (12, 'Human/Nature', 1),
                                                                 (13, 'Photo Story: Pushing the Limits', 1),
                                                                 (14, 'Cars', 58),
                                                                 (15, 'Anime', 64);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table photo_contest.contests
CREATE TABLE IF NOT EXISTS `contests` (
    `contest_id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(1000) NOT NULL,
    `category_id` int(11) DEFAULT NULL,
    `phase_id` int(11) DEFAULT 1,
    `organizer_id` int(11) DEFAULT NULL,
    `isOpen` tinyint(1) NOT NULL DEFAULT 0,
    `photo_id` int(11) NOT NULL,
    `phase_1_time_limit` datetime NOT NULL,
    `phase_2_time_limit` datetime DEFAULT NULL,
    `creation` datetime NOT NULL,
    `deleted` tinyint(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`contest_id`),
    UNIQUE KEY `contests_title_uindex` (`title`),
    KEY `contests_category__fk` (`category_id`),
    KEY `contests_phase__fk` (`phase_id`),
    KEY `contests_users_user_id_fk` (`organizer_id`),
    CONSTRAINT `contests_category__fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
    CONSTRAINT `contests_phase__fk` FOREIGN KEY (`phase_id`) REFERENCES `phases` (`phase_id`),
    CONSTRAINT `contests_users_user_id_fk` FOREIGN KEY (`organizer_id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.contests: ~9 rows (approximately)
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` (`contest_id`, `title`, `category_id`, `phase_id`, `organizer_id`, `isOpen`, `photo_id`, `phase_1_time_limit`, `phase_2_time_limit`, `creation`, `deleted`) VALUES
                                                                                                                                                                                       (15, 'Art Of Nature', 1, 1, 3, 1, 51, '2022-04-27 23:59:59', '2022-04-28 17:00:00', '2022-04-19 19:58:24', 0),
                                                                                                                                                                                       (16, 'Aquatic Life', 8, 1, 3, 1, 52, '2022-04-30 19:58:55', '2022-04-30 22:58:55', '2022-04-19 19:58:55', 0),
                                                                                                                                                                                       (17, 'Winged Life', 9, 1, 3, 1, 53, '2022-04-28 20:40:20', '2022-04-29 17:00:00', '2022-04-19 19:59:19', 0),
                                                                                                                                                                                       (18, 'Landscaped, Waterscapes, and Flora', 10, 1, 3, 1, 54, '2022-04-30 10:40:40', '2022-04-30 20:40:00', '2022-04-19 20:00:07', 0),
                                                                                                                                                                                       (19, 'Terrestrial Wildlife', 11, 1, 3, 1, 55, '2022-04-27 23:56:14', '2022-04-28 20:57:47', '2022-04-19 20:00:31', 0),
                                                                                                                                                                                       (20, 'Human / Nature', 12, 1, 3, 1, 56, '2022-05-24 20:56:21', '2022-05-25 14:57:56', '2022-04-19 20:01:00', 0),
                                                                                                                                                                                       (21, 'Pushing the limits', 13, 1, 3, 1, 57, '2022-05-04 10:36:25', '2022-05-04 23:59:59', '2022-04-19 20:02:38', 0),
                                                                                                                                                                                       (22, 'The art of vehicles', 14, 1, 3, 1, 60, '2022-04-30 23:41:30', '2022-05-01 16:25:25', '2022-04-19 20:11:06', 0),
                                                                                                                                                                                       (23, 'Anime ', 15, 1, 3, 0, 65, '2022-04-28 14:47:10', '2022-04-29 14:47:10', '2022-04-27 17:10:10', 0);
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;

-- Dumping structure for table photo_contest.jury
CREATE TABLE IF NOT EXISTS `jury` (
    `contest_id` int(11) NOT NULL,
    `user_id` int(11) NOT NULL,
    KEY `jury_contests_contest_id_fk` (`contest_id`),
    KEY `jury_users_user_id_fk` (`user_id`),
    CONSTRAINT `jury_contests_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`contest_id`),
    CONSTRAINT `jury_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.jury: ~5 rows (approximately)
/*!40000 ALTER TABLE `jury` DISABLE KEYS */;
INSERT INTO `jury` (`contest_id`, `user_id`) VALUES
                                                 (22, 17),
                                                 (18, 9),
                                                 (18, 32),
                                                 (18, 17),
                                                 (23, 2);
/*!40000 ALTER TABLE `jury` ENABLE KEYS */;

-- Dumping structure for table photo_contest.phases
CREATE TABLE IF NOT EXISTS `phases` (
    `phase_id` int(11) NOT NULL AUTO_INCREMENT,
    `phase` varchar(64) DEFAULT NULL,
    PRIMARY KEY (`phase_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.phases: ~3 rows (approximately)
/*!40000 ALTER TABLE `phases` DISABLE KEYS */;
INSERT INTO `phases` (`phase_id`, `phase`) VALUES
                                               (1, 'Phase I'),
                                               (2, 'Phase II'),
                                               (3, 'Finish');
/*!40000 ALTER TABLE `phases` ENABLE KEYS */;

-- Dumping structure for table photo_contest.photos
CREATE TABLE IF NOT EXISTS `photos` (
    `photo_id` int(11) NOT NULL AUTO_INCREMENT,
    `path` varchar(280) DEFAULT NULL,
    `name` varchar(280) DEFAULT NULL,
    `user_id` int(11) NOT NULL,
    PRIMARY KEY (`photo_id`),
    KEY `photos_users_user_id_fk` (`user_id`),
    CONSTRAINT `photos_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.photos: ~71 rows (approximately)
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` (`photo_id`, `path`, `name`, `user_id`) VALUES
                                                                 (1, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'kindpng_248253.png', 1),
                                                                 (31, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'fg1bsy.jpg', 1),
                                                                 (37, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'source-code-wallpaper-2560x1080_14.jpg', 1),
                                                                 (38, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', '5891352.jpg', 1),
                                                                 (39, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'desktop-backgrounds-nawpic-15.jpg', 1),
                                                                 (40, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'mdjsol1eiad11.jpg', 1),
                                                                 (41, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'desktop-backgrounds-nawpic-15.jpg', 1),
                                                                 (42, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'download.png', 1),
                                                                 (43, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 'rick-and-morty-wallpaper-portal-wallpaper-004.jpg', 1),
                                                                 (44, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 's-class-exterior-right-front-three-quarter-3.webp', 1),
                                                                 (45, 'C:\\Project\\Final Project\\./src/main/resources/static/img/', 's-class-exterior-right-front-three-quarter-3.webp', 1),
                                                                 (46, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\PhotoContest\\photo-contest\\./src/main/resources/static/img/', 'wallpaperflare.com_wallpaper.jpg', 3),
                                                                 (47, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\PhotoContest\\photo-contest\\./src/main/resources/static/img/', 'wallpaperflare.com_wallpaper.jpg', 2),
                                                                 (48, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\PhotoContest\\photo-contest\\./src/main/resources/static/img/', 'terrestrial wildlife.jpg', 3),
                                                                 (49, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'art of nature.jpg', 3),
                                                                 (50, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'art of nature.jpg', 3),
                                                                 (51, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'art of nature.jpg', 3),
                                                                 (52, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'aquatic life.jpg', 3),
                                                                 (53, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'Winged Life.jpg', 3),
                                                                 (54, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'landscapes waterscapes and flora.jpg', 3),
                                                                 (55, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'terrestrial wildlife.jpg', 3),
                                                                 (56, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'human-nature.jpg', 3),
                                                                 (57, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'push your limits.jpg', 3),
                                                                 (58, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'cars.jpg', 3),
                                                                 (59, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'cars.jpg', 3),
                                                                 (60, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'cars.jpg', 3),
                                                                 (61, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'kingofkingsStanislav.webp', 2),
                                                                 (62, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'ford-mustang-photo-contest.jpg', 11),
                                                                 (63, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'art of nature.jpg', 11),
                                                                 (64, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'anime.jpeg', 3),
                                                                 (65, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'anime.jpeg', 3),
                                                                 (66, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'freepictest.jpg', 17),
                                                                 (67, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'demon slayer.jpg', 9),
                                                                 (68, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '1.jpg', 9),
                                                                 (69, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '7.jpg', 9),
                                                                 (70, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '1.jpg', 9),
                                                                 (71, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '1.jpg', 9),
                                                                 (72, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '1.jpg', 9),
                                                                 (73, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '2.jpg', 2),
                                                                 (74, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '1.jpg', 2),
                                                                 (75, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '2.jpg', 2),
                                                                 (76, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '14.jpg', 2),
                                                                 (77, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', '1.jpg', 2),
                                                                 (78, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'bor.jpg', 9),
                                                                 (79, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'aquatic.jpg', 9),
                                                                 (80, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'pileta (1).jpg', 9),
                                                                 (81, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'jivotno (1).jpg', 9),
                                                                 (82, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'ddz.jpg', 9),
                                                                 (83, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'napun (1).jpg', 9),
                                                                 (84, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'kola (1).jpg', 9),
                                                                 (85, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'bleach.jpg', 9),
                                                                 (86, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'frozenhrast.jpg', 6),
                                                                 (87, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'beach.jpg', 6),
                                                                 (88, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'pileta (2).jpg', 6),
                                                                 (89, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'landscapes (1).jpg', 6),
                                                                 (90, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'jivotno (2).jpg', 6),
                                                                 (91, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'gorazelena.jpg', 6),
                                                                 (92, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'napun (2).jpg', 6),
                                                                 (93, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'bleach2.jpg', 6),
                                                                 (94, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'demon slayer3.jpg', 15),
                                                                 (95, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'kola (2).jpg', 15),
                                                                 (96, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'napun (3).jpg', 15),
                                                                 (97, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'dlphi.jpg', 15),
                                                                 (98, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'hrastotblizo.jpg', 15),
                                                                 (99, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'jivotno (3).jpg', 15),
                                                                 (100, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'one piece2.jpg', 21),
                                                                 (101, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'kola (13).jpg', 21),
                                                                 (102, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'napun (4).jpg', 21),
                                                                 (103, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'napun (5).jpg', 32),
                                                                 (104, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'one punch man.jpg', 32),
                                                                 (105, 'D:\\My Stuff\\TELERIK ACADEMY\\FinalProject\\New folder\\photo-contest\\./src/main/resources/static/img/', 'naruto.jpg', 36);
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;

-- Dumping structure for table photo_contest.rating
CREATE TABLE IF NOT EXISTS `rating` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `score` int(11) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.rating: ~11 rows (approximately)
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` (`id`, `score`) VALUES
                                         (0, 0),
                                         (1, 1),
                                         (2, 2),
                                         (3, 3),
                                         (4, 4),
                                         (5, 5),
                                         (6, 6),
                                         (7, 7),
                                         (8, 8),
                                         (9, 9),
                                         (10, 10);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;

-- Dumping structure for table photo_contest.submissions
CREATE TABLE IF NOT EXISTS `submissions` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) DEFAULT NULL,
    `contest_id` int(11) DEFAULT NULL,
    `photo_id` int(11) DEFAULT NULL,
    `title` varchar(60) DEFAULT NULL,
    `story` varchar(8000) DEFAULT NULL,
    `points` int(11) DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `submissions_contests_contest_id_fk` (`contest_id`),
    KEY `submissions_photos_photo_id_fk` (`photo_id`),
    KEY `submissions_users_user_id_fk` (`user_id`),
    CONSTRAINT `submissions_contests_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`contest_id`),
    CONSTRAINT `submissions_photos_photo_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`photo_id`),
    CONSTRAINT `submissions_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.submissions: ~28 rows (approximately)
/*!40000 ALTER TABLE `submissions` DISABLE KEYS */;
INSERT INTO `submissions` (`id`, `user_id`, `contest_id`, `photo_id`, `title`, `story`, `points`) VALUES
                                                                                                      (22, 9, 15, 78, 'Boron leaves', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (23, 9, 16, 79, 'Shark', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (24, 9, 17, 80, 'Red Bird on Fence', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (25, 9, 19, 81, 'Wild Leaf', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (26, 9, 20, 82, 'Out For a Smoke', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (27, 9, 21, 83, 'Feeling alive', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (28, 9, 22, 84, 'The Eyes Of Evil', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (29, 9, 23, 85, 'Ichigo Kurosaki', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (30, 6, 15, 86, 'Frozen In Time', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (31, 6, 16, 87, 'Rocky life', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (32, 6, 17, 88, 'Angry Chicken', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (33, 6, 18, 89, 'Perfect Place, Perfect Time', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
                                                                                                      (34, 6, 19, 90, 'Hello, It\'s Me', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(35, 6, 20, 91, 'One With The Forrest', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(36, 6, 21, 92, 'Almost To The Top', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(37, 6, 23, 93, 'The Devil in The Dark', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(38, 15, 23, 94, 'Yellow Flash', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(39, 15, 22, 95, 'Dream Team', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(40, 15, 21, 96, 'Almost There, Buddy', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(41, 15, 16, 97, 'Dolphins ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(42, 15, 15, 98, 'Leaves Breathing ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(43, 15, 19, 99, 'New Life Upon Us', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(44, 21, 23, 100, 'King Of The Pirates ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(45, 21, 22, 101, 'Classic On The Road', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(46, 21, 21, 102, 'Push!', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(47, 32, 21, 103, 'One Foot On The Ground', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(48, 32, 23, 104, 'One Punch Is All It Takes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0),
	(49, 36, 23, 105, 'Borthers ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tempor ligula. Morbi non dolor et lacus ullamcorper porta eu a risus. Aenean porta felis vel molestie mollis. In a sapien tincidunt, condimentum nulla congue, ullamcorper felis. Fusce convallis aliquet dolor, non faucibus mauris volutpat non. Aliquam sit amet velit rhoncus, auctor lectus sed, lobortis lorem. Morbi euismod vestibulum libero et condimentum. Integer eget orci sed nunc rhoncus molestie.', 0);
/*!40000 ALTER TABLE `submissions` ENABLE KEYS */;

-- Dumping structure for table photo_contest.submission_scoring
CREATE TABLE IF NOT EXISTS `submission_scoring` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `rating_id` int(11) DEFAULT 3,
  `user_id` int(11) NOT NULL,
  `comment` varchar(8000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `submission_scoring_rating_id_fk` (`rating_id`),
  KEY `submission_scoring_submissions_id_fk` (`submission_id`),
  KEY `submission_scoring_users_user_id_fk` (`user_id`),
  CONSTRAINT `submission_scoring_rating_id_fk` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`id`),
  CONSTRAINT `submission_scoring_submissions_id_fk` FOREIGN KEY (`submission_id`) REFERENCES `submissions` (`id`),
  CONSTRAINT `submission_scoring_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.submission_scoring: ~0 rows (approximately)
/*!40000 ALTER TABLE `submission_scoring` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_scoring` ENABLE KEYS */;

-- Dumping structure for table photo_contest.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `rank_id` int(11) NOT NULL DEFAULT 1,
  `points` int(11) NOT NULL DEFAULT 0,
  `creation` datetime DEFAULT current_timestamp(),
  `blocked` tinyint(1) DEFAULT 0,
  `photo_id` int(11) DEFAULT 2,
  `reset_password_token` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_username_uindex` (`username`),
  KEY `users_photos_photo_id_fk` (`photo_id`),
  KEY `users_role__fk` (`rank_id`),
  CONSTRAINT `users_photos_photo_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`photo_id`),
  CONSTRAINT `users_role__fk` FOREIGN KEY (`rank_id`) REFERENCES `user_rank` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.users: ~36 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email`, `rank_id`, `points`, `creation`, `blocked`, `photo_id`, `reset_password_token`) VALUES
	(1, 'Ivan', 'Nikolov', 'ivancho', '12345678', 'ivancho@abv.bg', 1, 421698, '2022-03-30 11:28:31', 0, 41, NULL),
	(2, 'Stanislav', 'Stanislav', 'stanislav', '12345678', 'stanislav@gmail.com', 4, 722, '2022-03-30 11:28:31', 0, 1, NULL),
	(3, 'Zhelyazko', 'Georgiev', 'jecho', '87654321', 'zheliazko1997@abv.bg', 1, 55566, '2022-03-31 11:46:23', 0, 1, NULL),
	(6, 'Alexandra', 'Velikova', 'sasha', '12345678', 'alexandra@gmail.com', 3, 59, '2022-04-18 23:47:56', 0, 1, NULL),
	(8, 'Angel', 'Giurov', 'angel', '12345678', 'angel@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(9, 'Ivan', 'Petkov', 'ivan.p', '12345678', 'ivan.p@gmail.com', 4, 204, '2022-04-19 00:01:50', 0, 1, NULL),
	(10, 'Martin', 'Karalev', 'martin', '12345678', 'martin@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(11, 'Plamenna', 'Pavlova', 'plamenna', '12345678', 'plamenna@gmail.com', 2, 2, '2022-04-19 00:01:50', 0, 1, NULL),
	(12, 'Tihomir', 'Dimitrov', 'tihomir', '12345678', 'tihomir@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(13, 'Dimitar', 'Petrov', 'dimitar', '12345678', 'dimitar@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(14, 'Ivo', 'Dimiev', 'ivo', '12345678', 'ivo@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(15, 'Stefan', 'Sirakov', 'stefan', '12345678', 'stefan@gmail.com', 2, 6, '2022-04-19 00:01:50', 0, 1, NULL),
	(16, 'Vilislav', 'Angelov', 'vilislav', '12345678', 'vilislav@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(17, 'Emiliyana', 'Kalinova', 'emiliyana', '12345678', 'emiliyana@gmail.com', 4, 721, '2022-04-19 00:01:50', 0, 1, NULL),
	(18, 'Georgi', 'Stukanyov', 'georgi.st', '12345678', 'georgi.st@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(19, 'Iskren', 'Danchev', 'iskren', '12345678', 'iskren@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(20, 'Ivan', 'Chepilev', 'ivan.ch', '12345678', 'ivan.ch@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(21, 'Ivaylo', 'Ivanov', 'ivaylo.iv', '12345678', 'ivaylo.iv@gmail.com', 4, 583, '2022-04-19 00:01:50', 0, 1, NULL),
	(22, 'Ivaylo', 'Stavrev', 'ivaylo.st', '12345678', 'ivaylo.st@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(23, 'Chavdar', 'Dimitrov', 'chavdar', '12345678', 'chavdar@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(24, 'Damian', 'Seizov', 'damian', '12345678', 'damian@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(25, 'Denislav', 'Vuchkov', 'denislav', '12345678', 'denislav@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(26, 'Lilia', 'Georgieva', 'lilia', '12345678', 'lilia@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(27, 'Martina', 'Dimova', 'martina', '12345678', 'martina@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(28, 'Niya', 'Staneva', 'niya', '12345678', 'niya@gmail.com', 5, 2530, '2022-04-19 00:01:50', 0, 1, NULL),
	(29, 'Todor', 'Bonev', 'todor', '12345678', 'todor@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(30, 'Yordan', 'Bardushev', 'yordan', '12345678', 'yordan@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(31, 'Martin', 'Todorov', 'martin.t', '12345678', 'martin.t@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(32, 'Sofi', 'Stoyanova', 'sofi', '12345678', 'sofi@gmail.com', 5, 1502, '2022-04-19 00:01:50', 0, 1, NULL),
	(33, 'Svetoslav', 'Hristov', 'svetoslav', '12345678', 'svetoslav@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(34, 'Andrey', 'Mitev', 'andrey', '12345678', 'andrey@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(35, 'Georgi', 'Krastev', 'georgi.kr', '12345678', 'georgi@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(36, 'Ivan', 'Malinov', 'ivan.m', '12345678', 'ivan.m@gmail.com', 2, 1, '2022-04-19 00:01:50', 0, 1, NULL),
	(37, 'Ivo', 'Bankov', 'ivo.b', '12345678', 'ivo.b@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(38, 'Plamena', 'Gospodinova', 'plamena', '12345678', 'plamena@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL),
	(39, 'Yoanna', 'Stoeva', 'yoana', '12345678', 'yoana@gmail.com', 2, 0, '2022-04-19 00:01:50', 0, 1, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table photo_contest.user_contests
CREATE TABLE IF NOT EXISTS `user_contests` (
  `user_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  KEY `user_contests_contests_contest_id_fk` (`contest_id`),
  KEY `user_contests_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_contests_contests_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`contest_id`),
  CONSTRAINT `user_contests_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.user_contests: ~36 rows (approximately)
/*!40000 ALTER TABLE `user_contests` DISABLE KEYS */;
INSERT INTO `user_contests` (`user_id`, `contest_id`) VALUES
	(11, 22),
	(11, 15),
	(17, 23),
	(9, 23),
	(9, 15),
	(9, 16),
	(9, 17),
	(9, 19),
	(9, 21),
	(2, 15),
	(2, 16),
	(2, 17),
	(2, 18),
	(2, 20),
	(9, 20),
	(9, 22),
	(6, 15),
	(6, 16),
	(6, 17),
	(6, 18),
	(6, 19),
	(6, 20),
	(6, 21),
	(6, 23),
	(15, 23),
	(15, 22),
	(15, 21),
	(15, 16),
	(15, 15),
	(15, 19),
	(21, 23),
	(21, 22),
	(21, 21),
	(32, 22),
	(32, 23),
	(36, 23);
/*!40000 ALTER TABLE `user_contests` ENABLE KEYS */;

-- Dumping structure for table photo_contest.user_rank
CREATE TABLE IF NOT EXISTS `user_rank` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.user_rank: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_rank` DISABLE KEYS */;
INSERT INTO `user_rank` (`role_id`, `role`) VALUES
	(1, 'Organizer'),
	(2, 'Junkie'),
	(3, 'Enthusiast'),
	(4, 'Master'),
	(5, 'Wise and Benevolent Photo Dictator');
/*!40000 ALTER TABLE `user_rank` ENABLE KEYS */;

-- Dumping structure for table photo_contest.winners
CREATE TABLE IF NOT EXISTS `winners` (
  `contest_id` int(11) NOT NULL,
  `submission_id` int(11) DEFAULT NULL,
  KEY `winners_contests_contest_id_fk` (`contest_id`),
  KEY `winners_submissions_id_fk` (`submission_id`),
  CONSTRAINT `winners_contests_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`contest_id`),
  CONSTRAINT `winners_submissions_id_fk` FOREIGN KEY (`submission_id`) REFERENCES `submissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table photo_contest.winners: ~0 rows (approximately)
/*!40000 ALTER TABLE `winners` DISABLE KEYS */;
/*!40000 ALTER TABLE `winners` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;