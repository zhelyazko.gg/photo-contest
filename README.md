# Photo Contest

## Content
- What is it
- Stack
- What functionalities does it have
- How to add the database to the project
- Creators


## What is it
This is a web-based application for a photo competition, which is built for a final project in Telerik Academy Alpha with Java program.\
In this application you can easily create and manage photo contests on different topics and categories.\
Every normal registered user can join a competition, this excludes the Organizers and the users who have been chosen to be part of the jury system in this contest.\
When a user joins a competition he no longer has permision to join again nor does he have permision to update his submission.\
Every contest has three phases:
- **First phase** has a duration from 1 day to 1 month and this is when everybody can add their submissions.
- **Second phase** has a duration from 1 hour to 1 day and this is when you cannot submit anymore pictures and the jury rates the submissions from phase one.
- **Third phase** is where the winners are announced.

## Stack
The technologies used for developing this application are:
- Java
- Hibernate
- Spring REST
- Thymeleaf
- Postman
- Mockito (for testing)
- HTML
- CSS
- JS
- MariaDB
  
## What functionalities does it have / doesn't have
- [x] Login;
- [x] Registration;
- [x] Update profile information (visible to everybody);
- [x] Update profile picture (visible to everybody);
- [x] Join Contest (visible for everyone except organizers and jury of the same contest);
- [x] View Submission (visible to everybody);
- [x] View only "Open" contests e.g Contests in phase 1(visible to people who haven't joined any contest);
- [x] View winners of contest (visible to everybody); 
- [x] Promote / Demote users to Organizer (functionality for organizers only);
- [x] Create contests (functionality for organizers only);
- [x] Rate submissions (functionality for organizers only);
- [x] Add jury to a contest  (functionality for organizers only);
- [x] View all contests (visible for organizers only);
- [x] View all users (visible for organizers only);
- [x] Forgotten Password;
- [ ] Invitational contests;

## How to add the database to the project
First you would like to have a registration on some kind of server (Heidi, MySQL Bench etc.)\
If you already have an account then here are the steps to import the database data:

Navigate yourself to the right of the screen and there you will see on the sidebar an option for "database", click on it.

![278169868_2185446521632054_7199125685940324790_n](/uploads/291af7f918a5a153b32242cc04651a44/278169868_2185446521632054_7199125685940324790_n.png)

First you would like to click on the "+" sign and then add a data source. You can choose whichever you are using. We are curerntly using MariaDB so that is what we will pick.

![278669542_521934339552787_163031934278883324_n](/uploads/283e1963bb81e8d50f46eefd77febd31/278669542_521934339552787_163031934278883324_n.png)

After that a window like this should pop out and what you have to do is to add your **username** and **password** to the sql server that you are using. After that click the "Test Connection" on the bottom to see if you have connection.

![278781747_1078048629415941_1358827496099308233_n](/uploads/d74cd19b6c1e3491f46a982fbbb8052e/278781747_1078048629415941_1358827496099308233_n.png)

After that you should have something like this pop up at the right of the screen where your database is. Right click on it.

![278874711_520543423132868_3280476145303497273_n](/uploads/802030c61c4db5b10767257059691b17/278874711_520543423132868_3280476145303497273_n.png)

You should have a menu like this. Click on "New" and then on "Scheme". Add a name for it or just click "OK".

![278402331_963793954324853_266685529842522874_n](/uploads/5b255792293bc3438b2262bf4beb7367/278402331_963793954324853_266685529842522874_n.png)

Now you should have some progress and this should be visible to you. Right click on the "scheme".

![278851397_287286643603878_1041847734601422406_n](/uploads/fc5b6ec8ca7c805f891159a00f608a33/278851397_287286643603878_1041847734601422406_n.png)

Then click on "New" -> "Query Console". There a console should pop on your screen. \
After that you have to copy the data from "tables.sql" which is located in the directory "db", select all of the data and press "Ctrl + Enter" to exceute the query. After that is complete get the data from "data.sql" and perform the same steps with the "tables.sql". \
Everything should be done. 

![279565191_347738687224483_3787742100200801439_n](/uploads/7d686f06ee2be891d9bee299d394802c/279565191_347738687224483_3787742100200801439_n.png)

This is how the database should look like.

## Creators
- **Ivan Nikolov**
- **Zhelyazko Georgiev**
